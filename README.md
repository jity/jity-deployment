README
======

[![Build Status](http://jenkins.jity.de/job/jity-deployment/badge/icon)](http://jenkins.jity.de/job/jity-deployment/)

What is the Jity Deployment Service?
------------------------------------

The Jity Deployment Service is a common Symfony2 application
to deploy any kind of applications to remote or local systems.

It can be used as a standalone application or as a extension
to phing. If it is used as an extension, all tasks will be available
to given build files.

Features
--------

##Deployment Application

* Highly customizeable
* Semantic configuration, only specify essentials and skip default values

##Configuration

* All paths will be identified and normalized by default
* All config values will be parsed for contatiner and runtime %parameters%
* An schemantic configuration can be dumped to list all default values and constains

##Fetcher

* Fetch the source distribution from serveral sources
    * Use the local source
    * Use a Git repository
    * Use a Svn repository

##Fixtures Loading

* Have a prebuild set of static files which will be needed for a certain environment?
  Just include these files to the fixtures collection and they will be copied over
  to the deployment archiv
* With this feature you can quickly manage multiple configuration files or any similar
  because the fixtures will overwrite the fetched files

##Installer Compiler

* Partial/Hooks can be inserted for
    * Base, Pre-Base, Post-Base
    * Environment, Pre-Environment, Post-Environment,
    * Specific slaves
* Validate installer syntax
* Print builded installer script for debugging and visualization

##Archiver

* Compressing algorithm for the package can be configured
    * gzip - fast processing, acceptable compression
    * bzip2 - acceptable processing, good compression
    * xz - acceptable processing, best compression

##Package & Installer Deployment

* All relevant files will be copied in parallel to the slaves
  for the current environment to speed up the process

##Package Installation

* All slaved will be installed in parallel to speed up the process
* If a slave got errors while processing they will be logged and displayed but
  they dont affect the installation of the other slaves

Usefull Links
-------------

* Documentation: http://docs.jity.de/deployment/
* API: http://api.jity.de/deployment/

