# Unzip project package
mkdir -p /tmp/package && tar xfv ${package_path} -C /tmp/package || exit 1;

# # Sync package with project
rsync -axzulptr --delete-after --numeric-ids --stats /tmp/package/ "${project_destination}" || exit 1;

# Do some basic after-sync logic
cd ${project_destination} || exit 1;

