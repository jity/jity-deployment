<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

$pathPath = __DIR__ . '/../../bin/deployment-console.phar';

try {
    Phar::loadPhar($pathPath);
    require 'phar://' . $pathPath . '/app/PhingExtension.php';
} catch (PharException $e) {
    echo 'Error while loading the phar archiv.' && exit;
}

require_once 'phing/Task.php';
require_once 'phing/Target.php';

/**
 * JityPhingBridge
 *
 * Load the Jity Phing extension.
 *
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class JityPhingBridge extends Task
{
    public function main()
    {
        $phingExt = new PhingExtension();
        $phingExt->setProject($this->project);
        $phingExt->init();
        $phingExt->main();

        $this->log('Extension loaded.');
    }
}

