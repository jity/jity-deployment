The Application in Pictures
---------------------------

Console and usefull Tools
^^^^^^^^^^^^^^^^^^^^^^^^^

The standalone Jity Deployment Application console:

.. image:: images/preview_console.png

List all configured slaves:

.. image:: images/preview_list_slaves.png

Dump an example configuration with all default values:

.. image:: images/preview_dump_example_config.png

Dump an compiled installer script:

.. image:: images/preview_dump_installer.png

An Deployment Process
^^^^^^^^^^^^^^^^^^^^^

First select the environment you want deploy to and the source from which
you will fetch the source-distribution:

.. image:: images/preview_deploy_slaves_1.png

After you validated the configuration, start the process:

.. image:: images/preview_deploy_slaves_2.png

Invalid installer script detected:

.. image:: images/preview_compiler_invalid.png

Diagrams
^^^^^^^^

The Jity Deployment Application structure:

.. image:: images/diagram_1.png

