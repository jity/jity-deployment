Introduction
------------

The Jity Deployment Service is a common Symfony2 application
to deploy any kind of applications to remote or local systems.
It can be used as an standalone application or as a extension
to phing. If it is used as a extension, all tasks will be available
to given build files.

Features
--------

Deployment Application
^^^^^^^^^^^^^^^^^^^^^^

* Highly customizeable
* Semantic configuration, only specify essentials and skip default values

Configuration
^^^^^^^^^^^^^

* All paths will be identified and normalized by default
* All config values will be parsed for contatiner and runtime %parameters%
* An schemantic configuration can be dumped to list all default values and constains

Fetcher
^^^^^^^

* Fetch the source distribution from serveral sources
    * Use the local source
    * Use a Git repository
    * Use a Svn repository

Fixtures Loading
^^^^^^^^^^^^^^^^

* Have a prebuild set of static files which will be needed for a certain environment?
  Just include these files to the fixtures collection and they will be copied over
  to the deployment archiv
* With this feature you can quickly manage multiple configuration files or any similar
  because the fixtures will overwrite the fetched files

Installer Compiler
^^^^^^^^^^^^^^^^^^

* Partial/Hooks can be inserted for
    * Base, Pre-Base, Post-Base
    * Environment, Pre-Environment, Post-Environment,
    * Specific slaves
* Validate installer syntax
* Print builded installer script for debugging and visualization

Archiver
^^^^^^^^

* Compressing algorithm for the package can be configured
    * gzip - fast processing, acceptable compression
    * bzip2 - acceptable processing, good compression
    * xz - acceptable processing, best compression

Package & Installer Deployment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* All relevant files will be copied in parallel to the slaves
  for the current environment to speed up the process

Package Installation
^^^^^^^^^^^^^^^^^^^^

* All slaved will be installed in parallel to speed up the process
* If a slave got errors while processing they will be logged and displayed but
  they dont affect the installation of the other slaves


Installation
------------

You can install the Jity Deployment Service in many different ways:

* Use the pre-build Phar executable
* Install it via Composer (``jity/deployment`` on `Packagist`_).
* Use the official Git repository (http://gitorious.hermann-mayer.net/jity/jity-deployment);

.. _Packagist: https://packagist.org/packages/jity/deployment

Installation via Installer-Script
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The installation via the installer-script is the recommended method.

**1.** Just run the following command from the root of your project:

.. code-block:: bash

    curl -s http://deployment.jity.de/installer | php

**2.** Prepare your project for the Jity Deployment with an minimal configuration:

.. code-block:: bash

    ./bin/deployment-console.phar prepare

Installation via Composer
^^^^^^^^^^^^^^^^^^^^^^^^^

This method is recommended if you want to develop on the Jity Deployment project.

**1.** If you're creating a new project, create a new empty directory for it.

**2.** Create a new file called ``composer.json`` and paste the following into it:

.. code-block:: json

    {
        "require": {
            "jity/deployment": "dev-master"
        }
    }

If you already have a ``composer.json`` file, just add this line to it. You
may also need to adjust the version (e.g. ``2.*``).

You can research the sub-projects of the Jity project and their versions at `packagist.org`_.

**3.** `Install composer`_ if you don't already have it present on your system: 

**4.** Download the vendor libraries and generate the ``vendor/autoload.php`` file:

.. code-block:: bash

    $ php composer.phar install

.. _Composer: http://getcomposer.org
.. _Install composer: http://getcomposer.org/download/
.. _packagist.org: https://packagist.org/

Installation via Git Submodule 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The installation via a Git submodule was used as the default
method for version 1.0. For completeness this section describes
the steps you need to do if you got a project which is managed by
git and you want to take all the hurdles with submodules.

In any case, this method is **not recommended**.

Just include it this way:

.. code-block:: bash

    $ git submodule add git@gitorious.hermann-mayer.net:jity/jity-deployment.git deploy
    $ git submodule init
    $ git submodule update

After this steps you can commit the inclusion to your repository.

