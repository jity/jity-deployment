<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Phing\Task;

use JMS\DiExtraBundle\Annotation as DI;
use Jity\DeployBundle\Phing\Task\AbstractTask;
use Jity\DeployBundle\Step\RuntimeInputs as RuntimeInputsStep;

/**
 * RuntimeInputs
 *
 * @DI\Service
 * @DI\Tag("jity.phing.task")
 *
 * @uses   AbstractTask
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class RuntimeInputs extends AbstractTask
{
    /**
     * __construct
     *
     * Initalize a new instance of the RuntimeInputs task.
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        $this->setInstance(new RuntimeInputsStep());
    }

    /**
     * main
     *
     * Adapter method to Phing's main method.
     * It is used to execute the step.
     *
     * @access public
     * @return voivd
     */
    public function main()
    {
        $config = \PhingExtension::getKernel()->getContainer()->get('jity.config');

        // Register all runtime inputs to the configration
        // This enables parameter replacement for inputed settings
        foreach (parent::main() as $name => $value) {
            $config->addRuntimeParameter($name, $value);
        }
    }
}

