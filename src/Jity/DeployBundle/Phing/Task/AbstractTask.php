<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Phing\Task;

use Jity\DeployBundle\Step\AbstractStep;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;

require_once 'phing/Task.php';

/**
 * AbstractStep
 *
 * A abstract task wrapper definition for Phing.
 *
 * @abstract
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
abstract class AbstractTask extends \Task
{
    protected $instance;
    protected $output;
    protected $topNewline    = true;
    protected $bottomNewline = false;


    /**
     * setInstance
     *
     * Set a given step to used and prepare.
     *
     * @param AbstractStep $step Step to prepare and use
     *
     * @access public
     * @return AbstractStep
     */
    public function setInstance(AbstractStep $step)
    {
        $this->setup();

        // Get static saved kernel->container from PhingExtension
        $container = \PhingExtension::getKernel()->getContainer();
        $config    = $container->get('jity.config');

        // Prepare this task
        $step->setContainer($container)
             ->setRuntimeInputs($config->getRuntimeParameters())
             ->setInput(new ArrayInput(array()))
             ->setOutput($this->output);

        $this->instance = $step;

        return $this;
    }

    /**
     * getName
     *
     * Get the converted AbstractStep name.
     * All dots of the AbstractStep name will be replaced by hyphens.
     *
     * @access public
     * @return string
     */
    public function getName()
    {
        return preg_replace('/\./', '-', 'jity.' . $this->instance->getName());
    }

    /**
     * getDescription
     *
     * Get the description of the AbstractStep.
     *
     * @access public
     * @return string
     */
    public function getDescription()
    {
        return $this->instance->getDescription();
    }

    /**
     * main
     *
     * Adapter method to Phing's main method.
     * It is used to execute the step.
     *
     * @access public
     * @return voivd
     */
    public function main()
    {
        $this->log($this->instance->getDescription(), \Project::MSG_INFO);

        if ($this->topNewline) {
            $this->instance->getOutput()->writeln('');
        }

        $result = $this->instance->validate();

        if (count($result) > 0) {

            foreach ($result as $exception) {

                $this->instance->getOutput()->writeln(
                    sprintf(
                        "<error>%s</error>",
                        $exception->getMessage()
                    )
                );
            }

            throw new \BuildException("Errors detected.");
        }

        // Add step to the cleanup queue
        \PhingExtension::addTaskToCleanupQueue($this->getName(), $this->instance);

        $result = $this->instance->execute();

        if ($this->bottomNewline) {
            $this->instance->getOutput()->writeln('');
        }

        return $result;
    }

    /**
     * setup
     *
     * Setup all requirements of a AbstractTask.
     *
     * @access public
     * @return void
     */
    protected function setup()
    {
        $this->output = new ConsoleOutput();

        $style = new OutputFormatterStyle('yellow', 'black');
        $this->output->getFormatter()->setStyle('h1', $style);

        $style = new OutputFormatterStyle('red', 'black');
        $this->output->getFormatter()->setStyle('h2', $style);

        $style = new OutputFormatterStyle('green', 'black');
        $this->output->getFormatter()->setStyle('h3', $style);

        $style = new OutputFormatterStyle('white', 'red', array('bold'));
        $this->output->getFormatter()->setStyle('error', $style);

        $style = new OutputFormatterStyle('white', 'green', array('bold'));
        $this->output->getFormatter()->setStyle('success', $style);

        $style = new OutputFormatterStyle('white', 'black', array('bold'));
        $this->output->getFormatter()->setStyle('br', $style);

        $style = new OutputFormatterStyle('white', 'black');
        $this->output->getFormatter()->setStyle('notice', $style);
    }

    /**
     * setTopNewline
     *
     * Set option to print a newline at top.
     *
     * @param boolean $value Value to set
     *
     * @access protected
     * @return AbstractTask
     */
    protected function setTopNewline($value)
    {
        $this->topNewline = (boolean) $value;

        return $this;
    }

    /**
     * setBottomNewline
     *
     * Set option to print a newline at bottom.
     *
     * @param boolean $value Value to set
     *
     * @access protected
     * @return AbstractTask
     */
    protected function setBottomNewline($value)
    {
        $this->bottomNewline = (boolean) $value;

        return $this;
    }
}

