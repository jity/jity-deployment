<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Phing\Task;

use JMS\DiExtraBundle\Annotation as DI;
use Jity\DeployBundle\Phing\Task\AbstractTask;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * Cleanup
 *
 * @DI\Service
 * @DI\Tag("jity.phing.task")
 *
 * @uses   AbstractTask
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class Cleanup extends AbstractTask
{
    /**
     * getName
     *
     * Get the converted Phing task name.
     *
     * @access public
     * @return string
     */
    public function getName()
    {
        return 'jity-cleanup';
    }

    /**
     * getDescription
     *
     * Get the description of this Phing task.
     *
     * @access public
     * @return string
     */
    public function getDescription()
    {
        return 'Run all cleanup routines for all used jity Phing tasks';
    }

    /**
     * init
     *
     * Initalize a new instance of the GenericFetcher task.
     *
     * @access public
     * @return void
     */
    public function main()
    {
        $output = new ConsoleOutput();

        $this->log('Run Cleanup Steps', \Project::MSG_INFO);
        $output->writeln('');

        // Get static saved services from PhingExtension
        $queue = \PhingExtension::getCleanupQueue();

        // Loop through the queue and execute the cleanup routines
        foreach ($queue as $step) {

            $step->cleanup();
        }
    }
}

