<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Phing\Task\Packaging\Fetcher;

use JMS\DiExtraBundle\Annotation as DI;
use Jity\DeployBundle\Phing\Task\AbstractTask;

/**
 * GenericFetcher
 *
 * @DI\Service
 * @DI\Tag("jity.phing.task")
 *
 * @uses   AbstractTask
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class GenericFetcher extends AbstractTask
{
    /**
     * getName
     *
     * Get the converted Phing task name.
     *
     * @access public
     * @return string
     */
    public function getName()
    {
        return 'jity-fetcher-inputs';
    }

    /**
     * getDescription
     *
     * Get the description of this Phing task.
     *
     * @access public
     * @return string
     */
    public function getDescription()
    {
        return 'Loads a concrete fetcher task by the specified settings from jity-runtime-inputs';
    }

    /**
     * init
     *
     * Initalize a new instance of the GenericFetcher task.
     *
     * @access public
     * @return void
     */
    public function init()
    {
        // Get static saved services from PhingExtension
        $container      = \PhingExtension::getKernel()->getContainer();
        $runtimeInputs  = $container->get('jity.config')->getRuntimeParameters();
        $fetcherHandler = $container->get('jity.handler.packaging.fetcher');

        $class = get_class(
            $fetcherHandler->get("fetcher." . $runtimeInputs['fetcher'])
        );

        $this->setInstance(new $class());
    }
}

