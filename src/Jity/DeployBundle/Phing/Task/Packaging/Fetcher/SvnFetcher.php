<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Phing\Task\Packaging\Fetcher;

use JMS\DiExtraBundle\Annotation as DI;
use Jity\DeployBundle\Phing\Task\AbstractTask;
use Jity\DeployBundle\Step\Packaging\Fetcher\SvnFetcher as SvnFetcherStep;

/**
 * SvnFetcher
 *
 * @DI\Service
 * @DI\Tag("jity.phing.task")
 *
 * @uses   AbstractTask
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class SvnFetcher extends AbstractTask
{
    /**
     * __construct
     *
     * Initalize a new instance of the SvnFetcher task.
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        $this->setInstance(new SvnFetcherStep());
    }
}

