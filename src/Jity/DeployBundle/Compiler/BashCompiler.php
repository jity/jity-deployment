<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Compiler;

use Symfony\Component\Process\ProcessBuilder;

/**
 * BashCompiler
 *
 * @uses   AbstractStep
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class BashCompiler extends AbstractCompiler
{
    /**
     * {@inheritdoc}
     */
    public function __construct($destination)
    {
        $this->language = 'bash';
        parent::__construct($destination);
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultStub()
    {
        return <<<'EOF'
#!/bin/bash
#
# This file is part of the Jity package.
#
# (c) Hermann Mayer <hermann.mayer92@gmail.com>
#
# For the full copyright and license information, please view the LICENSE
# file that was distributed with this source code.
#


EOF;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($file = '')
    {
        if (!$file) {
            $file = $this->destination;
        }

        $processBuilder = new ProcessBuilder(array(
            'bash', '-n', $file
        ));

        $this->validateWithProcess($processBuilder->getProcess(), $file);
    }
}

