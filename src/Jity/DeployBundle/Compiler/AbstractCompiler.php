<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Compiler;

use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Process\ProcessBuilder;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Process\Process;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * AbstractCompiler
 *
 * @DI\Service("jity.compiler")
 *
 * @uses   AbstractStep
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
abstract class AbstractCompiler
{
    const INDENT = '     ';

    protected $files;
    protected $stub;
    protected $destination;
    protected $language;
    protected $output;
    protected $outputPrefix = self::INDENT;

    /**
     * __construct
     *
     * Initalize a new AbstractCompiler instance.
     *
     * @param string $destination Path to the compiled destination
     *
     * @access public
     * @return void
     */
    public function __construct($destination)
    {
        $this->files       = array();
        $this->output      = new ConsoleOutput();
        $this->destination = $destination;
    }

    /**
     * setOutput
     *
     * Use a specific output instance.
     *
     * @param OutputInterface $output Output instance to write on
     *
     * @access public
     * @return AbstractCompiler
     */
    public function setOutput(OutputInterface $output)
    {
        $this->output = $output;

        return $this;
    }

    /**
     * setOutputPrefix
     *
     * Set an output prefix.
     *
     * @param string $prefix Prefix to set
     *
     * @access public
     * @return AbstractCompiler
     */
    public function setOutputPrefix($prefix)
    {
        $this->outputPrefix = $prefix;

        return $this;
    }

    /**
     * setStub
     *
     * Set a stub which will be used for compilation.
     *
     * @param string $stub
     *
     * @access public
     * @return AbstractCompiler
     */
    public function setStub($stub)
    {
        $this->stub = (string) $stub;

        return $this;
    }

    /**
     * getStub
     *
     * Return the configured stub.
     *
     * @access public
     * @return string
     */
    public function getStub()
    {
        return $this->stub;
    }

    /**
     * addFile
     *
     * Add a file to the collection.
     *
     * @param string|array $file File(s) to add
     *
     * @access public
     * @return AbstractCompiler
     */
    public function addFile($file)
    {
        if (is_array($file)) {
            $this->files = array_merge($this->files, $file);
        } else if (is_string($file)) {
            $this->files[] = $file;
        }

        return $this;
    }

    /**
     * getFiles
     *
     * Return all collected files.
     *
     * @access public
     * @return array
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * compile
     *
     * Compile the file collection with the stub.
     *
     * @access public
     * @return AbstractCompiler
     */
    public function compile()
    {
        file_put_contents($this->destination, $this->getDefaultStub());
        file_put_contents($this->destination, $this->stub, FILE_APPEND | LOCK_EX);

        foreach ($this->files as $file) {

            // Build concat process for all files
            $processBuilder = new ProcessBuilder(array(
                'eval', 'cat', $file, ' >> ', $this->destination
            ));

            $processBuilder->getProcess()->run();
        }

        return $this;
    }

    /**
     * dump
     *
     * Dump the compiled file. If we find ``highlight`` on the
     * environment we will use it to highlight the file.
     *
     * @access public
     * @return void
     */
    public function dump()
    {
        if (empty($this->language)) {
            return;
        }

        // Determinate if we got highlight installed
        $processBuilder = new ProcessBuilder(array(
            'which', 'highlight'
        ));

        $process = $processBuilder->getProcess();
        $process->run();

        // If errors occured, we dont have highlight
        if (!$process->isSuccessful()) {

            $outputProcessBuilder = new ProcessBuilder(array(
                'cat', $this->destination
            ));

        } else {

            $outputProcessBuilder = new ProcessBuilder(array(
                'highlight', '--style=zenburn',
                '--out-format=xterm256', '--syntax=' . $this->language,
                $this->destination
            ));
        }

        $outputProcess = $outputProcessBuilder->getProcess();
        $outputProcess->run();

        $this->output->write($outputProcess->getOutput());
    }

    /**
     * validateWithProcess
     *
     * Validate the destination file with the help of a process.
     *
     * @param Process $process Process to run for validation
     * @param string  $file    File-path which will be displayed
     *
     * @access protected
     * @return void
     */
    protected function validateWithProcess(Process $process, $file)
    {
        $process->run();

        if (!$process->isSuccessful()) {

            $this->output->writeln(array(
                sprintf($this->outputPrefix . "<br>[</br> <error> Invalid  </error> <br>]</br> > <notice>%s</notice> ", $file),
                '',
                sprintf('<error>%s</error>', $process->getErrorOutput())
            ));

            if (0 != $errors) {
                throw new \RuntimeException(
                    'Compiled File Validation found errors.'
                );
            }

        } else {

            $this->output->writeln(
                sprintf($this->outputPrefix . "<br>[</br> <success>  Valid   </success> <br>]</br> > <notice>Validate %s</notice> ", $file)
            );
        }
    }

    /**
     * getDefaultStub
     *
     * Return the default stub for a specific language compiler.
     *
     * @abstract
     * @access public
     * @return string
     */
    public abstract function getDefaultStub();


    /**
     * validate
     *
     * Validate the compiled file or an given one.
     *
     * @param string $file File to validate
     *
     * @abstract
     * @access public
     * @return void
     */
    public abstract function validate($file = '');
}

