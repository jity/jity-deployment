<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * ListSlavesCommand
 *
 * Dump current slave configuration.
 *
 * @uses   ContainerAwareCommand
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class ListSlavesCommand extends BaseCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('list-slaves')
            ->setDescription('List all configured slaves for deployment');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $config = $this->getContainer()->get('jity.config');
        $logger = $this->getContainer()->get('logger');

        $output->writeln('The following environments / slaves are configured:');

        foreach ($config->get('slaves') as $env => $slaves) {

            $output->writeln(array(
                '',
                '<br>[</br> <h1>' . $env . '</h1> <br>]</br>'
            ));

            foreach ($slaves as $name => $slave) {

                $output->writeln(array(
                    '',
                    '    <br>[</br><h2>' . $name . '</h2><br>]</br>',
                    '        Host: ' . $slave->host
                    . '<comment>:' . $slave->port . '</comment>',
                    '        Path: ' . $slave->projectDestination
                ));
            }

            $logger->addError(
                'Found ' . count($slaves) . ' slaves for '
                . $env . ' environment.'
            );
        }
    }
}

