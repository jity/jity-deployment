<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Jity\DeployBundle\Step\ExecutionManager;
use Symfony\Component\Process\ProcessBuilder;
use Jity\DeployBundle\Step\RuntimeInputs;
use Symfony\Component\Filesystem\Filesystem;

/**
 * DeployCommand
 *
 * Deploy source distribution to configured slaves
 * with a defined workflow.
 *
 * @uses   ContainerAwareCommand
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class DeployCommand extends BaseCommand
{
    private $start;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('deploy')
            ->setDescription('Deploy source distribution to all configured slaves');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->start = time();

        $dialog = $this->getDialogHelper();
        $config = $this->getContainer()->get('jity.config');

        $dialog->printHeader($output, $this->getContainer());

        $envs           = $config->get('environments');
        $defaultEnv     = $config->get('defaults.environment');
        $defaultSource  = $config->get('defaults.source');
        $fetcherHandler = $this->getContainer()->get('jity.handler.packaging.fetcher');

        // Choose env and source interactive
        if (false === $input->hasParameterOption(array('--no-interaction', '-n'))) {

            // Get runtimeInputs step
            $runtimeInputsStep = $this->getContainer()->get('jity.deploy.runtime.inputs');

            // Prepare the step
            $runtimeInputsStep->setContainer($this->getContainer())
                              ->setInput($input)
                              ->setOutput($output);

            $runtimeInputs = $runtimeInputsStep->execute();

        } else {

            // Use defaults as picked if we avoid interaction
            // $pickedEnvironment = $defaultEnv;
            // $pickedFetcher     = $defaultSource;

            // Build options array for step preparing
            // Use defaults as picked if we avoid interaction
            $runtimeInputs = array(
                'environment' => $defaultEnv,
                'fetcher'     => $defaultSource
            );
        }

        // Register all runtime inputs to the configration
        // This enables parameter replacement for inputed settings
        foreach ($runtimeInputs as $name => $value) {
            $config->addRuntimeParameter($name, $value);
        }

        // Create the deployment cache dir
        $filesystem = new Filesystem();
        $cacheDir   = $this->getContainer()->getParameter('jity.cache_dir');
        $filesystem->mkdir($cacheDir);

        // Display the configration and let the user confirm
        $this->printConfiguration($input, $output, $runtimeInputs);

        // Build the workflow by adding all steps to a queue
        $execManager = new ExecutionManager(
            $input,
            $output,
            $this->getContainer(),
            $this->getDialogHelper()
        );

        // Add a fetcher step
        $execManager->add(
            $fetcherHandler->get("fetcher." . $runtimeInputs['fetcher'])
        );

        // Add the fixtures loader step
        $execManager->add(
            $this->getContainer()->get('jity.deploy.packaging.fixtures.loader')
        );

        // Add the fixtures loader step
        $execManager->add(
            $this->getContainer()->get('jity.deploy.packaging.installer.compiler')
        );

        // Add the archiver step
        $execManager->add(
            $this->getContainer()->get('jity.deploy.packaging.archiver')
        );

        // Add the package deployer step
        $execManager->add(
            $this->getContainer()->get('jity.deploy.installation.package.deployer')
        );

        // Add the package installer step
        $execManager->add(
            $this->getContainer()->get('jity.deploy.installation.package.installer')
        );

        // Execute the defined workflow
        $execManager->prepare($runtimeInputs);
        // $execManager->validate();
        $execManager->run();

        // Clear out jity cache directory
        $filesystem->remove($cacheDir);

        // Print summary
        $this->printSummary($output);
    }

    /**
     * printConfiguration
     *
     * Print configration for deployment and ask for confirmation.
     *
     * @param InputInterface  $input         Input Instance to use
     * @param OutputInterface $output        Output Instance to use
     * @param array           $runtimeInputs Entered runtime inputs to use
     *
     * @access private
     * @return void
     */
    private function printConfiguration(InputInterface $input, OutputInterface $output, array $runtimeInputs)
    {
        $dialog = $this->getDialogHelper();
        $config = $this->getContainer()->get('jity.config');
        $slaves = $config->get('slaves.' . $runtimeInputs['environment']);

        // Prepare all messages to display
        $messages = array(
            'Get the source from'          => $runtimeInputs['fetcher'],
            'Environment for deployment'   => $runtimeInputs['environment'],
            'Slaves found for environment' => count((array) $slaves),
        );

        // Add slaves list
        foreach ($slaves as $name => $slave) {

            $messages["+ $name"] = sprintf(
                "<notice>%s:</notice><h2>%d</h2>",
                $slave->host, $slave->port
            );
        }

        // Some calculations with project size
        // First get project size
        $processBuilder = new ProcessBuilder(array(
            'du', '-skh',
            $config->get(
                'packaging.fetcher.sources.local.source.'
                . $runtimeInputs['environment'] . '.path'
            )
        ));

        $process = $processBuilder->getProcess();
        $process->run();

        preg_match('/([0-9]+)([A-Z]+)/', $process->getOutput(), $size);
        $projectSize    = $size[0];
        $compressedSize = ($size[1] * 0.2) . $size[2] . ' (approx.)';

        // Add messages for this information
        $messages['Size of the project']                           = $projectSize;
        $messages['Size of the compressed archiv'] = $compressedSize;

        // Write configration section
        $dialog->writeSection($output, 'Current Deployment Configuration');
        $output->writeln($dialog->formatMessages($messages));
        $dialog->writeSeperatorLine($output, true);

        // Only ask for confirmation if we are on interaction mode
        if (false === $input->hasParameterOption(array('--no-interaction', '-n'))) {

            // Ask confirmation
            $answer = $dialog->askConfirmation(
                $output,
                ' Check the configuration. Are you sure to start the process? [Y/n] : '
            );

            $output->writeln('');

            if (true !== $answer) {
                $output->writeln('Process aborted.');
                exit;
            }
        }
    }

    /**
     * printSummary
     *
     * Print summary of the process.
     *
     * @param OutputInterface $output Output Instance to use
     *
     * @access private
     * @return void
     */
    private function printSummary(OutputInterface $output)
    {
        $config = $this->getContainer()->get('jity.config');
        $dialog = $this->getDialogHelper();
        $end    = time();

        $memoryUsed = function($size) {
            $unit = array('B','KB','MB','GB','TB','PB');

            return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . $unit[$i];
        };

        $environment  = $config->getRuntimeParameter('environment');
        $slavesAmount = count($config->get("slaves.${environment}"));
        $packageSize  = $config->getRuntimeParameter('package.size') * $slavesAmount;

        // Prepare all messages to display
        $messages = array(
            'Processed in'     => gmdate('H:i:s', ($end - $this->start)),
            'Memory Usage'     => $memoryUsed(memory_get_usage(true)),
            'Transferred Data' => $memoryUsed($packageSize),
        );

        // Write summary section
        $output->writeln('');
        $dialog->writeSection($output, 'Process Statistics and Summary');
        $output->writeln($dialog->formatMessages($messages));
        $dialog->writeSeperatorLine($output);
    }
}

