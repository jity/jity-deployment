<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

require_once __DIR__ . '/../../../../app/PhingExtension.php';

/**
 * ListPhingTasksCommand
 *
 * Dump all available Phing Tasks.
 *
 * @uses   ContainerAwareCommand
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class ListPhingTasksCommand extends BaseCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('list-phing-tasks')
            ->setDescription('List all available Phing tasks and their description');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Bootstrap a Phing-like environment to run further
        $phingExtension = new \PhingExtension();
        $phingExtension->init();

        $phingTaskHandler = $this->getContainer()->get('jity.handler.phing.task');
        $phingTasks       = $phingTaskHandler->getAllRegistered();

        // Sort Phing tasks
        ksort($phingTasks);

        $output->writeln(array('The following Phing tasks are available:', ''));

        // Loop through registered phing tasks print them out
        foreach ($phingTasks as $name => $task) {

            $output->writeln(array(
                sprintf('<h1>## %s</h1>', $name),
                sprintf('Description: %s', $task->getDescription()),
                sprintf('    Example: <h2>\<%s /></h2>', $name),
                '',
                ''
            ));
        }
    }
}

