<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Config\Definition\ReferenceDumper;
use Jity\DeployBundle\DependencyInjection\Configuration\Deploy\ConfigStatic;
use Jity\DeployBundle\DependencyInjection\Configuration\Deploy\ConfigDynamic;
use Symfony\Component\Process\ProcessBuilder;

/**
 * DumpExampleConfigCommand
 *
 * Dump the default configuration for the application.
 *
 * @uses   ContainerAwareCommand
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class DumpExampleConfigCommand extends BaseCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('dump-example-config')
            ->setDescription('Dump the default configuration for the deployment application');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $staticConfig  = new ConfigStatic($this->getContainer());
        $dynamicConfig = new ConfigDynamic();
        $refDumper     = new ReferenceDumper();
        $slaves        = explode("\n", $refDumper->dump($dynamicConfig));
        $ref           = explode("\n", $refDumper->dump($staticConfig));

        // Indent usefull slaves def
        foreach ($slaves as &$row) {
            $row = '    ' . $row;
        }

        // Replace empty slaves def with usefull one
        foreach ($ref as &$row) {
            if (false !== strpos($row, 'slaves')) {
                $row = implode("\n", $slaves);
            }
        }

        // Determinate if we got highlight installed
        $process = new ProcessBuilder(array(
            'which', 'highlight'
        ));
        $process = $process->getProcess();
        $process->run();

        // If errors occured, we dont have highlight
        if (!$process->isSuccessful()) {

            $output->writeln($ref);

        } else {

            $outputProcess = new ProcessBuilder(array(
                'eval', 'echo', '"' . implode("\n", $ref) . '"', '|', 'highlight', '--style=zenburn', '--out-format=xterm256', '--syntax=css'
            ));
            $outputProcess = $outputProcess->getProcess();
            $outputProcess->run();

            $output->writeln($outputProcess->getOutput());
        }
    }
}

