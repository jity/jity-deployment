<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Command\Helper;

use Symfony\Component\Console\Helper\DialogHelper as BaseDialogHelper;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * DialogHelper
 *
 * Extended DialogHelper class.
 *
 * @uses   BaseDialogHelper
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class DialogHelper extends BaseDialogHelper
{
    /**
     * askSelect
     *
     * Ask for a configured value to choose from a list.
     *
     * @param OutputInterface $output   Output Instance to use
     * @param string          $question Question to ask
     * @param array           $choices  Choices to list
     * @param string          $default  Default value
     *
     * @access public
     * @return void
     */
    public function askSelect(OutputInterface $output, $question, array $choices, $default = null)
    {
        $messages = array(
            $this->getQuestion($question),
        );

        foreach ($choices as $key => $choice) {
            $messages[] = sprintf('   [%d] %s', $key, $choice);
        }

        $output->writeln($messages);

        $validation = function ($picked) use ($choices) {

            if (!in_array($picked, array_values($choices))) {

                throw new \InvalidArgumentException(sprintf(
                    '"%s" is invalid.', $picked
                ));
            }

            return $picked;
        };

        $result = $this->askAndValidate(
            $output, $this->getQuestion(PHP_EOL . ' >', $default), $validation, false, $default, $choices
        );

        $output->writeln('');

        return $result;
    }

    /**
     * getQuestion
     *
     * Format a question.
     *
     * @param string $question Question to ask
     * @param string $default  Default value
     * @param string $sep      Seperator to use
     *
     * @access public
     * @return string
     */
    public function getQuestion($question, $default = null, $sep = ':')
    {
        return $default
            ? sprintf(' <info>%s</info> [<comment>%s</comment>]%s ', $question, $default, $sep)
            : sprintf(' <info>%s</info>%s ', $question, $sep);
    }

    /**
     * formatMessages
     *
     * Format messages array with the format:
     *
     * array(
     *      'message text' => 'value'
     * )
     *
     * @param array $messages Messages to format
     *
     * @access public
     * @return array
     */
    public function formatMessages(array $messages)
    {
        $formatedMessages = array();
        foreach ($messages as $message => $value) {

            // Refresh max width, because we modify it sometimes
            $width = max(array_map('strlen', array_keys($messages)));

            if ('+' == $message[0]) {
                $message = sprintf('<comment>%s</comment>', $message);
                $width += 19;
            }

            $formatedMessages[] = sprintf(
                " | %${width}s: <br>%s</br>",
                $message, $value
            );
        }

        return $formatedMessages;
    }

    /**
     * writeSection
     *
     * Write a section header.
     *
     * @param OutputInterface $output  Output Instance to use
     * @param string          $section Name of the section
     * @param boolean         $newline Print empty line at the end
     *
     * @access public
     * @return void
     */
    public function writeSection(OutputInterface $output, $section, $newline = false)
    {
        $line = str_repeat('-', strlen($section)+2);

        $this->writeSeperatorLine($output);

        $output->writeln(array(
            sprintf(' | <comment>%s:</comment>', $section),
            ' +' . $line,
        ));

        if (true === $newline) {
            $output->writeln('');
        }
    }

    /**
     * writeSeperatorLine
     *
     * Write a section seperator line.
     *
     * @param OutputInterface $output  Output Instance to use
     * @param boolean         $newline Print empty line at the end
     * @param int             $length  Length of the line
     *
     * @access public
     * @return void
     */
    public function writeSeperatorLine(OutputInterface $output, $newline = false, $length = 63)
    {
        $output->writeln(' +' . str_repeat('-', $length) . '+ ');

        if (true === $newline) {
            $output->writeln('');
        }
    }

    /**
     * printHeader
     *
     * Print the header of the command.
     *
     * @param OutputInterface    $output    Output Instance to use
     * @param ContainerInterface $container Container to use
     * @param boolean            $frame     Print frame
     *
     * @access public
     * @return void
     */
    public function printHeader(OutputInterface $output, ContainerInterface $container, $frame = true)
    {
        // Clear the terminal to remove all disturbing content
        system('clear');

        $version = $container->getParameter('jity.version');

        if (true === $frame) {

            $output->writeln(array(
                ' +---------------------------------------------------------------+ ',
                " | <comment>    _ _ _          ___           _                        _   </comment>",
                " | <comment> _ | (_) |_ _  _  |   \ ___ _ __| |___ _  _ _ __  ___ _ _| |_ </comment>",
                " | <comment>| || | |  _| || | | |) / -_) '_ \ / _ \ || | '  \/ -_) ' \  _|</comment>",
                " | <comment> \__/|_|\__|\_, | |___/\___| .__/_\___/\_, |_|_|_\___|_||_\__|</comment>",
                " | <comment>            |__/           |_|         |__/</comment>  <notice>Version:</notice> <h2>${version}</h2>",
                ' +---------------------------------------------------------------+ ',
                ''
            ));

        } else {

            $output->writeln(array(
                "<comment>    _ _ _          ___           _                        _   </comment>",
                "<comment> _ | (_) |_ _  _  |   \ ___ _ __| |___ _  _ _ __  ___ _ _| |_ </comment>",
                "<comment>| || | |  _| || | | |) / -_) '_ \ / _ \ || | '  \/ -_) ' \  _|</comment>",
                "<comment> \__/|_|\__|\_, | |___/\___| .__/_\___/\_, |_|_|_\___|_||_\__|</comment>",
                "<comment>            |__/           |_|         |__/</comment>  <notice>Version:</notice> <h2>${version}</h2>",
            ));
        }
    }
}

