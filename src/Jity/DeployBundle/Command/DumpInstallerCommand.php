<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Jity\DeployBundle\Step\ExecutionManager;
use Symfony\Component\Process\ProcessBuilder;
use Jity\DeployBundle\Compiler\BashCompiler;

/**
 * DumpInstallerCommand
 *
 * Dump a installer script for a specified environment/slave.
 *
 * @uses   ContainerAwareCommand
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class DumpInstallerCommand extends BaseCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('dump-installer')
            ->setDescription('Dump the installer script for a environment/slave')
            ->addArgument(
                'env',
                InputArgument::REQUIRED,
                'Compile installer scripts for which environment?'
            )
            ->addArgument(
                'slave',
                InputArgument::REQUIRED,
                'Dump installer script for which slave?'
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dialog = $this->getDialogHelper();
        $config = $this->getContainer()->get('jity.config');
        $envs   = $config->get('environments');

        $pickedEnvironment = $input->getArgument('env');

        if (!in_array($pickedEnvironment, $envs)) {

            throw new \InvalidArgumentException(sprintf(
                'Environment "%s" is invalid. Registered environments are: [%s]',
                $pickedEnvironment,
                implode(', ', $envs)
            ));
        }

        $pickedSlave = $input->getArgument('slave');
        $slaves      = array_keys((array) $config->get("slaves.$pickedEnvironment"));

        if (!in_array($pickedSlave, $slaves)) {

            throw new \InvalidArgumentException(sprintf(
                'Slave "%s" is invalid. Registered slaves for environment "%s" are: [%s]',
                $pickedSlave,
                $pickedEnvironment,
                implode(', ', $slaves)
            ));
        }

        // Build options array for step preparing
        $runtimeInputs = array(
            'environment' => $pickedEnvironment
        );

        // Register all runtime inputs to the configration
        // This enables parameter replacement for inputed settings
        foreach ($runtimeInputs as $name => $value) {
            $config->addRuntimeParameter($name, $value);
        }

        $installerCompiler = $this->getContainer()->get('jity.deploy.packaging.installer.compiler');

        $installerCompiler->setContainer($this->getContainer())
                          ->setInput($input)
                          ->setOutput($output)
                          ->setRuntimeInputs($runtimeInputs);

        $dialog->writeSection($output, 'Run Installer Compiler', true);

        $installerCompiler->execute();

        $runtimeParams = $config->getRuntimeParameters();

        $output->writeln('');
        $dialog->writeSection($output, 'Dump Installer Script', true);

        // Setup new bash compiler and add found hooks
        $compiler = new BashCompiler($runtimeParams['installer'][$pickedSlave]);
        $compiler->setOutput($output);

        $compiler->dump();
    }
}

