<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Jity\DeployBundle\Command\Helper\DialogHelper;

/**
 * BaseCommand
 *
 * Container aware base command to extend for custom needs.
 *
 * @uses   ContainerAwareCommand
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
abstract class BaseCommand extends ContainerAwareCommand
{
    /**
     * getDialogHelper
     *
     * Shortcut for getting the dialog helper.
     *
     * @access protected
     * @return DialogHelper
     */
    protected function getDialogHelper()
    {
        $dialog = $this->getHelperSet()->get('dialog');
        if (!$dialog || get_class($dialog) !== 'Jity\DeployBundle\Command\Helper\DialogHelper') {
            $this->getHelperSet()->set($dialog = new DialogHelper());
        }

        return $dialog;
    }
}

