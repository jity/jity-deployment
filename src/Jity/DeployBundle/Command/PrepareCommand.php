<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Config\Definition\ReferenceDumper;
use Jity\DeployBundle\DependencyInjection\Configuration\Deploy\ConfigStatic;
use Jity\DeployBundle\DependencyInjection\Configuration\Deploy\ConfigDynamic;
use Symfony\Component\Process\ProcessBuilder;
use Symfony\Component\Filesystem\Filesystem;

/**
 * PrepareCommand
 *
 * Prepare a deployment directory for a given path.
 *
 * @uses   ContainerAwareCommand
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class PrepareCommand extends BaseCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('prepare')
            ->setDescription('Prepare a deployment directory for your application')
            ->addArgument(
                'working-dir',
                InputArgument::OPTIONAL,
                'Working directory which we will use'
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $destination = $input->getArgument('working-dir');

        if ($destination) {

            if (!file_exists($destination) || !is_dir($destination)) {
                throw new \RuntimeException(sprintf('The working directory "%s" does not exist.', $destination));
            }

        } else {
            $destination = getcwd();
        }

        $destination .= '/deployment';
        $rootDir      = $this->getContainer()->getParameter('kernel.root_dir');

        $fs = new Filesystem();
        $fs->mkdir($destination);

        $dirs = array(
            'logs',
            'tasks',
            'hooks',
            'hooks/fetcher',
            'hooks/fetcher/prod',
            'hooks/fetcher/prod/local',
            'hooks/fetcher/prod/git',
            'hooks/fetcher/prod/base',
            'hooks/fetcher/demo',
            'hooks/fetcher/demo/local',
            'hooks/fetcher/demo/git',
            'hooks/fetcher/demo/base',
            'hooks/fetcher/base',
            'hooks/fetcher/base/local',
            'hooks/fetcher/base/git',
            'hooks/archiver',
            'hooks/archiver/prod',
            'hooks/archiver/demo',
            'hooks/archiver/base',
            'hooks/installer',
            'hooks/installer/prod',
            'hooks/installer/prod/slaves',
            'hooks/installer/prod/pre',
            'hooks/installer/prod/post',
            'hooks/installer/demo',
            'hooks/installer/demo/slaves',
            'hooks/installer/demo/pre',
            'hooks/installer/demo/post',
            'hooks/installer/base',
            'hooks/installer/base/pre',
            'hooks/installer/base/post',
            'fixtures',
            'fixtures/prod',
            'fixtures/demo',
            'phing-bridge',
        );

        foreach ($dirs as $dir) {
            $fs->mkdir($destination . '/' . $dir);
        }

        $fs->mirror($rootDir . '/../docs/example-config', $destination);
    }
}

