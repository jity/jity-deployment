<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Jity\DeployBundle\DependencyInjection\Compiler\AbstractCompilerPass;

/**
 * JityDeployBundle
 *
 * The core Jity project bundle class.
 *
 * @uses   Bundle
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class JityDeployBundle extends Bundle
{
    /**
     * build
     *
     * Initalize the bundle and register new compiler passes.
     *
     * @param ContainerBuilder $container Dependency Injection Container
     *
     * @access public
     * @return void
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        // Add current working dir parameter
        $container->setParameter('working.dir', getcwd());

        // Build deployment cache dir
        $kernelCacheDir = $container->getParameter('kernel.cache_dir');
        $deployCacheDir = realpath($kernelCacheDir . '/../') . '/deploy-' . uniqid();
        $container->setParameter('jity.cache_dir', $deployCacheDir);

        // Set Jity config directory
        $container->setParameter('jity.config_dir', getcwd() . '/deployment');

        // Add Handler for tagged extensions
        $container->addCompilerPass(
            new AbstractCompilerPass(
                'jity.handler.extension.task',
                'jity.deploy.extension.task'
            )
        );

        // Add Handler for tagged fetcher steps
        $container->addCompilerPass(
            new AbstractCompilerPass(
                'jity.handler.packaging.fetcher',
                'jity.deploy.packaging.fetcher'
            )
        );

        // Add Handler for tagged Phing tasks
        $container->addCompilerPass(
            new AbstractCompilerPass(
                'jity.handler.phing.task',
                'jity.phing.task'
            )
        );
    }
}

