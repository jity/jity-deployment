<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Step;

use JMS\DiExtraBundle\Annotation as DI;
use Jity\DeployBundle\Step\AbstractStep;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Jity\DeployBundle\Command\Helper\DialogHelper;

/**
 * RuntimeInputs
 *
 * @DI\Service("jity.deploy.runtime.inputs")
 *
 * @uses   AbstractStep
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class RuntimeInputs extends AbstractStep
{
    /**
     * __construct
     *
     * @access public
     * @return void
     */
    public function configure()
    {
        $this
            ->setName('runtime.inputs')
            ->setDescription('Ask for environment and source settings');
    }

    /**
     * getConfiguration
     *
     * Define the configration for the step.
     *
     * @abstract
     * @access public
     * @return Symfony\Component\Config\Definition\Builder\TreeBuilder
     */
    public function getConfiguration()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode    = $treeBuilder->root('runtimeInputs');

        return $rootNode;
    }

    /**
     * validate
     *
     * Validate the running conditions of the step.
     * If we dont find problems, we could run and return
     * an empty array, else we will return a array with exceptions.
     *
     * @access public
     * @return array
     */
    public function validate()
    {
        return array();
    }

    /**
     * execute
     *
     * Execute statements for the step.
     *
     * @abstract
     * @access public
     * @return array
     */
    public function execute()
    {
        $dialog = new DialogHelper();
        $config = $this->getConfig();

        $envs           = $this->getConfig()->get('environments');
        $defaultEnv     = $this->getConfig()->get('defaults.environment');
        $defaultSource  = $this->getConfig()->get('defaults.source');
        $fetcherHandler = $this->getContainer()->get('jity.handler.packaging.fetcher');

        // Ask for the environment
        $pickedEnvironment = $dialog->askSelect(
            $this->getOutput(),
            'Choose the destination environment',
            $envs,
            (in_array($defaultEnv, $envs)) ? $defaultEnv : $envs[0]
        );

        $fetcherSources = $config->get('packaging.fetcher.sources');

        // Map all configured sources
        $configuredSources = array_keys((array) $fetcherSources);

        // Map all available sources
        $availableSources = array();

        foreach ($fetcherHandler->getAllRegistered() as $fetcher) {
            $name = explode('.', $fetcher->getName());
            $availableSources[] = $name[1];
        }

        $selectableSources = array_values(array_intersect($availableSources, $configuredSources));

        // Ask for the fetcher source
        $pickedFetcher = $dialog->askSelect(
            $this->getOutput(),
            'Choose the source to fetch',
            $selectableSources,
            (in_array($defaultSource, $selectableSources)) ? $defaultSource : $selectableSources[0]
        );

        // Return options array for step preparing
        return array(
            'environment' => $pickedEnvironment,
            'fetcher'     => $pickedFetcher
        );
    }
}


