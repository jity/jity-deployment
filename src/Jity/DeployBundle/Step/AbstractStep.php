<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Step;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\ProcessBuilder;
use Symfony\Component\Process\Process;
use Jity\DeployBundle\Step\Helper\ProcessHelper;

/**
 * AbstractStep
 *
 * A abstract and common valid step definition.
 *
 * @abstract
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
abstract class AbstractStep implements ContainerAwareInterface
{
    const INDENT = '     ';

    protected $name;
    protected $description;
    protected $preProcess;
    protected $postProcess;
    protected $cleanup;
    protected $container;
    protected $runtimeInputs;
    protected $input;
    protected $output;
    protected $outputPrefix = ' ';
    protected $filesystem;
    protected $helper = array();


    /**
     * __construct
     *
     * Initalize a new AbstractStep instance.
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        $this->configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function getContainer()
    {
        return $this->container;
    }

    /**
     * {@inheritdoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;

        return $this;
    }

    /**
     * setInput
     *
     * Set the input instance for the step.
     *
     * @param InputInterface $input Input Instance to set
     *
     * @access public
     * @return AbstractStep
     */
    public function setInput(InputInterface $input)
    {
        $this->input = $input;

        return $this;
    }

    /**
     * getInput
     *
     * Get the specified input instance of the step.
     *
     * @access public
     * @return InputInterface
     */
    public function getInput()
    {
        return $this->input;
    }

    /**
     * setOutput
     *
     * Set the output instance for the step.
     *
     * @param OutputInterface $output Output Instance to set
     *
     * @access public
     * @return AbstractStep
     */
    public function setOutput(OutputInterface $output)
    {
        $this->output = $output;

        return $this;
    }

    /**
     * getOutput
     *
     * Get the specified output instance of the step.
     *
     * @access public
     * @return OutputInterface
     */
    public function getOutput()
    {
        return $this->output;
    }

    /**
     * setRuntimeInputs
     *
     * Set the runtime inputs for the step.
     *
     * @param array $runtimeInputs Runtime Inputs to set
     *
     * @access public
     * @return AbstractStep
     */
    public function setRuntimeInputs(array $runtimeInputs)
    {
        $this->runtimeInputs = $runtimeInputs;

        return $this;
    }

    /**
     * getRuntimeInputs
     *
     * Get the specified runtime inputs of the step.
     *
     * @access public
     * @return array
     */
    public function getRuntimeInputs()
    {
        return $this->runtimeInputs;
    }

    /**
     * setName
     *
     * Set the name for the step.
     *
     * @param string $name Name to set
     *
     * @access public
     * @return AbstractStep
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * getName
     *
     * Get the specified name for the step.
     *
     * @access public
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * setDescription
     *
     * Set the description for the step.
     *
     * @param string $description Description to set
     *
     * @access public
     * @return AbstractStep
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * getDescription
     *
     * Get the specified description for the step.
     *
     * @access public
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * setOutputPrefix
     *
     * Set the output prefix for the step.
     *
     * @param string $prefix Prefix to set
     *
     * @access public
     * @return AbstractStep
     */
    public function setOutputPrefix($prefix)
    {
        $this->outputPrefix = $prefix;

        return $this;
    }

    /**
     * getOutputPrefix
     *
     * Get the specified output prefix for the step.
     *
     * @access public
     * @return string
     */
    public function getOutputPrefix()
    {
        return $this->outputPrefix;
    }

    /**
     * setPreProcess
     *
     * Set a pre process closure for the step.
     *
     * @param \Closure $closure Closure to set
     *
     * @access public
     * @return AbstractStep
     */
    public function setPreProcess(\Closure $closure)
    {
        if (!is_callable($closure)) {
            throw new \InvalidArgumentException(
                'Invalid callable provided to AbstractStep::setCleanup.'
            );
        }

        $this->preProcess = $closure;

        return $this;
    }

    /**
     * hasPreProcess
     *
     * Return a boolean value to represent we got a pre process closure.
     *
     * @access public
     * @return boolean
     */
    public function hasPreProcess()
    {
        return (null === $this->preProcess) ? false : true;
    }

    /**
     * executePreProcess
     *
     * Execute the pre process statements for the step.
     *
     * @access public
     * @return mixed
     */
    public function executePreProcess()
    {
        return (null === $this->preProcess) ? null : call_user_func($this->preProcess);
    }

    /**
     * setPostProcess
     *
     * Set a pre process closure for the step.
     *
     * @param \Closure $closure Closure to set
     *
     * @access public
     * @return AbstractStep
     */
    public function setPostProcess(\Closure $closure)
    {
        if (!is_callable($closure)) {
            throw new \InvalidArgumentException(
                'Invalid callable provided to AbstractStep::setCleanup.'
            );
        }

        $this->postProcess = $closure;

        return $this;
    }

    /**
     * hasPostProcess
     *
     * Return a boolean value to represent we got a post process closure.
     *
     * @access public
     * @return boolean
     */
    public function hasPostProcess()
    {
        return (null === $this->postProcess) ? false : true;
    }

    /**
     * executePostProcess
     *
     * Execute the post process statements for the step.
     *
     * @access public
     * @return mixed
     */
    public function executePostProcess()
    {
        return (null === $this->postProcess) ? null : call_user_func($this->postProcess);
    }

    /**
     * setCleanup
     *
     * Set a cleanup closure for the step.
     *
     * @param \Closure $closure Closure to set
     *
     * @access public
     * @return AbstractStep
     */
    public function setCleanup(\Closure $closure)
    {
        if (!is_callable($closure)) {
            throw new \InvalidArgumentException(
                'Invalid callable provided to AbstractStep::setCleanup.'
            );
        }

        $this->cleanup = $closure;

        return $this;
    }

    /**
     * hasCleanup
     *
     * Return a boolean value to represent we got a cleanup closure.
     *
     * @access public
     * @return boolean
     */
    public function hasCleanup()
    {
        return (null === $this->cleanup) ? false : true;
    }

    /**
     * cleanup
     *
     * Execute the cleanup statements for the step.
     *
     * @access public
     * @return mixed
     */
    public function cleanup()
    {
        return (null === $this->cleanup) ? null : call_user_func($this->cleanup);
    }

    /**
     * getFilesystem
     *
     * Shortcut for get a Filesystem instance.
     *
     * @access public
     * @return Filesystem
     */
    public function getFilesystem()
    {
        if (null === $this->filesystem) {
            $this->filesystem = new Filesystem();
        }

        return $this->filesystem;
    }

    /**
     * getConfig
     *
     * Get configration from dependency container.
     *
     * @access public
     * @return StdClass
     */
    public function getConfig()
    {
        return $this->getContainer()->get('jity.config');
    }

    /**
     * getDeployEnvironment
     *
     * Shortcut for getting the deploy environment from runtime inputs.
     *
     * @access public
     * @return string
     */
    public function getDeployEnvironment()
    {
        return $this->runtimeInputs['environment'];
    }

    /**
     * getProcessHelper
     *
     * Shortcut for getting the process helper.
     *
     * @access public
     * @return ProcessHelper
     */
    public function getProcessHelper()
    {
        if (!array_key_exists('process', $this->helper)) {

            $this->helper['process'] = new ProcessHelper(
                $this->getOutput(),
                $this->getConfig(),
                $this->getContainer()->get('logger')
            );

            $this->helper['process']->setName($this->getName());
        }

        return $this->helper['process'];
    }

    /**
     * isEmptyDirectory
     *
     * Checks if a specified directory is empty or not.
     *
     * @param string $path Path to check
     *
     * @access public
     * @return boolean
     */
    public function isEmptyDirectory($path)
    {
        // Check if we have a valid installer source path
        if (true === file_exists($path) && true === is_dir($path)) {

            $files = scandir($path);

            // 2 files are default => '.' and '..'
            if (count($files) > 2) {
                return false;
            }
        }

        return true;
    }

    /**
     * getHookPaths
     *
     * Return all locations to look for hooks for the current step.
     * This method can be overwritten if used.
     *
     * @access public
     * @return array
     */
    public function getHookPaths()
    {
        return array();
    }

    /**
     * configure
     *
     * Configure the step, eg setting metadata.
     *
     * @abstract
     * @access public
     * @return void
     */
    abstract public function configure();

    /**
     * validate
     *
     * Validate the running conditions of the step.
     * If we dont find problems, we could run and return
     * an empty array, else we will return a array with exceptions.
     *
     * @access public
     * @return array
     */
    abstract public function validate();

    /**
     * getConfiguration
     *
     * Define the configration for the step.
     *
     * @abstract
     * @access public
     * @return Symfony\Component\Config\Definition\Builder\TreeBuilder
     */
    abstract public function getConfiguration();

    /**
     * execute
     *
     * Execute statements for the step.
     *
     * @abstract
     * @access public
     * @return void
     */
    abstract public function execute();
}

