<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Step\Helper;

use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\ProcessBuilder;
use Symfony\Component\Process\Process;

/**
 * ProcessHelper
 *
 * Helps building and running processes.
 *
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class ProcessHelper
{
    private $output;
    private $config;
    private $logger;
    private $name;


    /**
     * __construct
     *
     * @param OutputInterface $output Output instance to use
     * @param mixed           $config Jity config to use
     * @param mixed           $logger Logger to use
     *
     * @access public
     * @return void
     */
    public function __construct(OutputInterface $output, $config, $logger)
    {
        $this->output = $output;
        $this->config = $config;
        $this->logger = $logger;
    }

    /**
     * setName
     *
     * Set the name which will be logged on errors.
     *
     * @param string $name Name to set
     *
     * @access public
     * @return ProcessHelper
     */
    public function setName($name)
    {
        $this->name = (string) $name;

        return $this;
    }

    /**
     * buildProcess
     *
     * Shortcut for process building.
     *
     * @param array $arguments Arguments to pass to the process builder
     *
     * @access public
     * @return Process
     */
    public function buildProcess($arguments = array())
    {
        $builder = new ProcessBuilder($arguments);

        return $builder->getProcess();
    }

    /**
     * getProcessBuilder
     *
     * Shortcut for get a ProcessBuilder instance.
     *
     * @param array $arguments Arguments to pass to the ProcessBuilder
     *
     * @access public
     * @return ProcessBuilder
     */
    public function getProcessBuilder($arguments = array())
    {
        return new ProcessBuilder($arguments);
    }

    /**
     * run
     *
     * Process execution helper to run one or many processes
     * in a blocking or a non-blocking way with adhoc verbosity
     * or not.
     *
     * @param array|Process $processes   Processes to run
     * @param boolean       $verbose     Print stdout/err to commandline
     * @param boolean       $parallel    Start processes parallel (non-blocking way)
     * @param boolean       $dieOnErrors Stop execution after processing all if error occured
     * @param Closure       $output      Closure to attach to a blocking process for adhoc output handling
     *
     * @access public
     * @return null|array
     */
    public function run($processes, $verbose = false, $parallel = false, $dieOnErrors = true, Closure $output = null)
    {
        $remainingProcesses = array();
        $finishedProcesses  = array();

        // We can perform multiple processes with one call
        // so just analyse all inputs
        if ($processes instanceof Process) {

            $remainingProcesses[] = $processes;

        } else if (is_array($processes)) {

            foreach ($processes as $process) {

                if ($process instanceof Process) {
                    $remainingProcesses[] = $process;
                } else {
                    throw new \RuntimeException(
                        'A object in array is not a Process instance.'
                    );

                    return;
                }
            }
        }

        // Dont go further if we haven't any process to run
        if (empty($remainingProcesses)) {

            throw new \RuntimeException('No Process instance(s) found to run.');

            return;
        }

        // Set default output closure for adhoc process outputs
        if (null === $output) {

            $output = function ($type, $data) {

                if (Process::ERR == $type) {

                    // $data was sent to the error output
                    $this->output->writeln("<error>$data</error>");

                } else {

                    // $data was sent to the standard output
                    $this->output->writeln("<notice>$data</notice>");
                }
            };
        }

        $outputsToReturn = array();
        $out             = '';
        $err             = '';

        // Run all processes queued up to run
        // until no process remains
        while (count($remainingProcesses) > 0) {

            foreach ($remainingProcesses as $i => $process) {

                // Start the current process with the
                // specified parameters
                if (!$process->isStarted()) {

                    if (true === $this->config->get('defaults.printUsedCommands')) {

                        $this->output->writeln(
                            self::INDENT . '> <comment>' . $process->getCommandLine() . '</comment>'
                        );
                    }

                    if (true === (boolean) $parallel) {

                        // Just run the process in an non-blocking way
                        // while processing the output adhoc
                        $process->start();

                        continue;
                    }

                    // Just run the process in an non-blocking way
                    // while processing the output adhoc
                    if (true === (boolean) $verbose) {
                        $process->run($output);
                    } else {
                        $process->run();
                    }

                    continue;
                }

                // Non-blocking parallel processing needs special output
                // routine if verbose is set
                if (true === (boolean) $verbose && true === (boolean) $parallel) {

                    $this->output->write(sprintf(
                        '<notice>%s</notice>', $process->getIncrementalOutput()
                    ));

                    $this->output->write(sprintf(
                        '<error>%s</error>', $process->getIncrementalErrorOutput()
                    ));
                }

                // Run in parallel without being verbose seems buggy, because
                // we will never get a exit so "isRunning" is always true
                if (false === (boolean) $verbose && true === (boolean) $parallel) {

                    $out .= $process->getIncrementalOutput();
                    $err .= $process->getIncrementalErrorOutput();
                }

                // The current process finished we just remove it
                // from the remaining process list and move it
                // to the finished process list
                if (!$process->isRunning()) {

                    unset($remainingProcesses[$i]);
                    $finishedProcesses[$i] = $process;
                }
            }
        }

        // Evaluate the status of all runned processes
        foreach ($finishedProcesses as $i => $process) {

            if (!$process->isSuccessful()) {

                // Log the error and abort processing
                $this->logger->addError(
                    sprintf('[%s]', $this->name) . $process->getErrorOutput()
                );

                if (true === (boolean) $dieOnErrors) {
                    throw new \RuntimeException($process->getErrorOutput());
                }
            }

            // Save outputs
            $outputsToReturn[$i]['output'] = $process->getOutput();
            $outputsToReturn[$i]['error'] = $process->getErrorOutput();
        }

        return $outputsToReturn;
    }
}

