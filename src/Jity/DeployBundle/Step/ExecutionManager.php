<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Step;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Jity\DeployBundle\Command\Helper\DialogHelper;
use Symfony\Component\Finder\Finder;
use Jity\DeployBundle\Compiler\BashCompiler;
use Symfony\Component\Filesystem\Filesystem;
use Jity\DeployBundle\Step\Helper\ProcessHelper;

/**
 * ExecutionManager
 *
 * Simple execution queue processor.
 *
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class ExecutionManager
{
    private $executionQueue;
    private $cleanupQueue;
    protected $runtimeInputs;
    protected $input;
    protected $output;
    protected $container;


    /**
     * __construct
     *
     * Initalize a new ExecutionManager instance.
     *
     * @param InputInterface     $input        Command Input Instance
     * @param OutputInterface    $output       Command Output Instance
     * @param ContainerInterface $container    DI Container
     * @param DialogHelper       $dialogHelper Dialog Helper to pretty print stuff
     *
     * @access public
     * @return void
     */
    public function __construct(InputInterface $input, OutputInterface $output, ContainerInterface $container, DialogHelper $dialogHelper)
    {
        $this->executionQueue = array();
        $this->cleanupQueue   = array();
        $this->input          = $input;
        $this->output         = $output;
        $this->container      = $container;
        $this->dialog         = $dialogHelper;
    }

    /**
     * add
     *
     * Add a AbstractStep to the execution queue.
     *
     * @param AbstractStep $step Step to add to queue
     *
     * @access public
     * @return ExecutionManager
     */
    public function add(AbstractStep $step)
    {
        // $formatedName = '<br>[</br> <h2>' .  . '</h2> <br>]</br>';

        $this->executionQueue[$step->getName()] = $step;

        if (true === $step->hasCleanup()) {
            $this->cleanupQueue[] = $step;
        }

        return $this;
    }

    /**
     * prepare
     *
     * Setup all steps.
     *
     * @param array $runtimeInputs Runtime Inputs entered by the user
     *
     * @access public
     * @return void
     */
    public function prepare(array $runtimeInputs)
    {
        $this->dialog->writeSection($this->output, 'Prepare and Validate Steps', true);
        $this->runtimeInputs = $runtimeInputs;

        $width         = max(array_map('strlen', array_keys($this->executionQueue)));
        $preparedQueue = array();

        // Prepare every step in the queue
        foreach ($this->executionQueue as $formatedName => $step) {

            // Reformat the name
            $formatedName = sprintf(
                "<br>[</br><h2> %-${width}s </h2><br>]</br>",
                $formatedName
            );

            // Populate step environment
            $step->setContainer($this->container)
                 ->setRuntimeInputs($this->runtimeInputs)
                 ->setInput($this->input)
                 ->setOutput($this->output);

            // Validate the step
            $this->validateStep($formatedName, $step);

            // Setup hooks for the step
            $this->setupHookClosureForStep($formatedName, $step);

            // Add step to prepared queue
            $preparedQueue[$formatedName] = $step;
        }

        $this->executionQueue = $preparedQueue;
        $this->output->writeln('');
    }

    /**
     * run
     *
     * Run all steps in the execution queue.
     *
     * @access public
     * @return void
     */
    public function run()
    {
        $this->dialog->writeSection($this->output, 'Run Deployment Workflow', true);

        $width      = max(array_map('strlen', array_keys($this->executionQueue)));
        $amount     = count($this->executionQueue);
        $widthIndex = strlen($amount)*2+1;
        $i          = 0;

        foreach ($this->executionQueue as $name => $step) {

            $prefix = sprintf(
                " <br>[</br> <comment>%${widthIndex}s</comment> <br>]</br> %-${width}s  ",
                ++$i . '/' . $amount, $name
            );

            $step->setOutputPrefix($prefix);
            $this->output->writeln(sprintf(
                "%s%s",
                $prefix, $step->getDescription()
            ));

            $step->executePreProcess();
            $step->execute();
            $step->executePostProcess();
        }

        $this->output->writeln('');
        $this->dialog->writeSection($this->output, 'Run Cleanup Steps', true);

        foreach ($this->cleanupQueue as $step) {
            $step->cleanup();
            $this->output->writeln(sprintf(
                ' [ <success>  Done  </success> ] Run cleanup for Step <h2>%s</h2>',
                $step->getName()
            ));
        }
    }

    /**
     * validateStep
     *
     * Validate all queued steps to prevent in process errors.
     *
     * @param string       $formatedName Formated name to display
     * @param AbstractStep $step         Step to validate
     *
     * @access public
     * @return void
     */
    private function validateStep($formatedName, $step)
    {
        $result = $step->validate();
        $count  = count($result);

        if ($count > 0) {

            foreach ($result as $exception) {
                $this->output->writeln(sprintf(
                    " %s <br>[</br> <error> Invalid </error> <br>]</br> <error>%s</error>",
                    $formatedName,
                    $exception->getMessage()
                ));
            }

            throw new \RuntimeException('Not all steps are ready to run. Check the error messages.');

        } else {
            $this->output->writeln(sprintf(
                " %s <br>[</br> <success>  Valid  </success> <br>]</br>",
                $formatedName
            ));
        }
    }

    /**
     * setupHookClosureForStep
     *
     * Setup the closures for a step.
     *
     * @param string       $formatedName Formated name to display
     * @param AbstractStep &$step        Step to populate
     *
     * @access private
     * @return void
     */
    private function setupHookClosureForStep($formatedName, &$step)
    {
        // Dont process if nothing is to do
        if (0 == count($step->getHookPaths())) {
            return;
        }

        $hookPath = sprintf(
            '%s/hooks/%s.sh',
            $this->container->getParameter('jity.cache_dir'),
            $step->getName()
        );

        $filesystem = new Filesystem();
        $filesystem->mkdir(dirname($hookPath));

        // Build usefull closure additions
        $config        = $this->container->get('jity.config');
        $logger        = $this->container->get('logger');
        $processHelper = new ProcessHelper(
            $this->output, $config, $logger
        );
        $processHelper->setName($step->getName() . '-hook');

        // Loop through all defined hooks
        foreach ($step->getHookPaths() as $hookExecution => $hookPaths) {

            // Parse hook paths first
            foreach ($hookPaths as &$path) {
                $path = $config->parse($path);
            }

            // Try to find hooks for given step
            $finder = Finder::create()
                ->files()
                ->name('*.sh')
                ->depth('<= 0')
                ->in($hookPaths);

            if (0 == count($finder)) {
                continue;
            }

            $prefix = ' ' . $formatedName . ' <br>[</br> <question>  Hooks  </question> <br>]</br> ';

            // Setup new bash compiler and add found hooks
            $compiler = new BashCompiler($hookPath);
            $compiler->setOutput($this->output)
                     ->setOutputPrefix($prefix);

            // Add a files to the compilation
            foreach ($finder as $hook) {
                $compiler->addFile($hook->getPathname());
            }

            $compiler->compile();
            $compiler->validate();

            // Build the closure
            $closure = function() use ($hookPath, $config, $filesystem, $processHelper) {

                $hookName     = basename($hookPath);
                $hookDestPath = $config->get('packaging.fetcher.destination');
                $hookDest     = $hookDestPath . $hookName;

                $filesystem->copy($hookPath, $hookDest);

                $process = $processHelper->buildProcess(array(
                    'bash', '-c', "cd $hookDestPath && bash ./$hookName"
                ));

                $processHelper->run($process, $config->get('defaults.verboseHooks'));
                $filesystem->remove($hookDest);
            };

            // Finally set the closures for the process
            if ('pre' == $hookExecution) {
                $step->setPreProcess($closure);
            } else if ('post' == $hookExecution) {
                $step->setPostProcess($closure);
            }
        }
    }
}

