<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Step\Packaging;

use JMS\DiExtraBundle\Annotation as DI;
use Jity\DeployBundle\Step\AbstractStep;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Process\Process;

/**
 * FixturesLoader
 *
 * @DI\Service("jity.deploy.packaging.fixtures.loader")
 *
 * @uses   AbstractStep
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class FixturesLoader extends AbstractStep
{
    /**
     * __construct
     *
     * @access public
     * @return void
     */
    public function configure()
    {
        $this
            ->setName('fixtures.loader')
            ->setDescription('Copy over fixtures to the source distribution');
    }

    /**
     * getConfiguration
     *
     * Define the configration for the step.
     *
     * @abstract
     * @access public
     * @return Symfony\Component\Config\Definition\Builder\TreeBuilder
     */
    public function getConfiguration()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode    = $treeBuilder->root('fixturesLoader');

        $rootNode
            ->addDefaultsIfNotSet()
            ->children()
                ->scalarNode('source')
                    ->defaultValue('%working.dir%/deployment/fixtures/%environment%')
                ->end()
                ->scalarNode('extraCliArgs')
                    ->defaultValue(null)
                ->end()
                ->scalarNode('verbose')
                    ->defaultFalse()
                ->end()
            ->end();

        return $rootNode;
    }

    /**
     * validate
     *
     * Validate the running conditions of the step.
     * If we dont find problems, we could run and return
     * an empty array, else we will return a array with exceptions.
     *
     * @access public
     * @return array
     */
    public function validate()
    {
        // This step is optional, if we find
        // an invalid source path we will skip
        return array();
    }

    /**
     * execute
     *
     * Execute statements for the step.
     *
     * @abstract
     * @access public
     * @return void
     */
    public function execute()
    {
        $sourcePath = $this->getConfig()->get('packaging.fixturesLoader.source');

        // Check if we have a valid fixtures path
        if (false === file_exists($sourcePath)) {

            $this->getOutput()->writeln(array(
                '',
                $this->getOutputPrefix() . '> <comment>No fixtures copied - Directory does not exists: </comment>',
                $this->getOutputPrefix() . "> <br>$sourcePath</br>"
            ));

            return;
        }

        // Build local copy process
        $procBuilder = $this->getProcessHelper()->getProcessBuilder(array(
            'cp', '-a', '-v'
        ));

        $extraArgs = $this->getConfig()->get(
            'packaging.fixturesLoader.extraCliArgs'
        );

        if (null !== $extraArgs) {

            // Add extra process arguments
            $procBuilder->add($extraArgs);
        }

        $procBuilder
            ->add($sourcePath . '.')
            ->add($this->getConfig()->get('packaging.fetcher.destination'));

        // Use abstract process runner helper
        $this->getProcessHelper()->run(
            $procBuilder->getProcess(),
            $this->getConfig()->get('packaging.fixturesLoader.verbose')
        );
    }
}

