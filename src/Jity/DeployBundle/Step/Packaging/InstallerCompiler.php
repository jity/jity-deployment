<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Step\Packaging;

use JMS\DiExtraBundle\Annotation as DI;
use Jity\DeployBundle\Step\AbstractStep;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Process\Process;
use Jity\DeployBundle\Compiler\BashCompiler;
use Symfony\Component\Finder\Finder;

/**
 * InstallerCompiler
 *
 * @DI\Service("jity.deploy.packaging.installer.compiler")
 *
 * @uses   AbstractStep
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class InstallerCompiler extends AbstractStep
{
    /**
     * __construct
     *
     * @access public
     * @return void
     */
    public function configure()
    {
        $this
            ->setName('installer.compiler')
            ->setDescription('Build installer scripts for all configured slaves of the environment')
            ->setCleanup(function() {

                // Remove the fetcher destination
                $this->getFilesystem()->remove(
                    $this->getConfig()->get('packaging.installerCompiler.destination')
                );
            });
    }

    /**
     * getConfiguration
     *
     * Define the configration for the step.
     *
     * @abstract
     * @access public
     * @return Symfony\Component\Config\Definition\Builder\TreeBuilder
     */
    public function getConfiguration()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode    = $treeBuilder->root('installerCompiler');

        $rootNode
            ->addDefaultsIfNotSet()
            ->children()
                ->scalarNode('source')
                    ->defaultValue('%working.dir%/deployment/hooks/installer')
                ->end()
                ->scalarNode('destination')
                    ->defaultValue('%jity.cache_dir%/installer')
                ->end()
                ->booleanNode('printUsedPartials')
                    ->defaultValue(true)
                ->end()
                ->booleanNode('validateInstaller')
                    ->defaultValue(true)
                ->end()
            ->end();

        return $rootNode;
    }

    /**
     * validate
     *
     * Validate the running conditions of the step.
     * If we dont find problems, we could run and return
     * an empty array, else we will return a array with exceptions.
     *
     * @access public
     * @return array
     */
    public function validate()
    {
        $errors = array();

        $sourcePath = $this->getConfig()
            ->get('packaging.installerCompiler.source');

        // Check if we have a valid installer source path
        if (false === file_exists($sourcePath)) {

            $errors[] = new \RuntimeException(sprintf(
                'Source Path "%s" does not exist.', $sourcePath
            ));
        }

        return $errors;
    }

    /**
     * execute
     *
     * Execute statements for the step.
     *
     * @abstract
     * @access public
     * @return void
     */
    public function execute()
    {
        $sourcePath = $this->getConfig()
            ->get('packaging.installerCompiler.source');

        $destinationPath = $this->getConfig()
            ->get('packaging.installerCompiler.destination');

        // Check if we have a valid installer destination path
        if (false === file_exists($destinationPath)) {

            // Create the destination if needed
            $this->getFilesystem()->mkdir(
                $destinationPath
            );
        }

        $basePartialCollection = array();
        $hookSearchPaths       = array();
        $installerPaths        = array();
        $compilerStack         = array();

        $slaves = $this->getConfig()->get('slaves.%environment%');

        // Build paths by convention
        $hookPaths = array(
            'base/pre/',
            'base/',
            '%environment%/pre/',
            '%environment%/',
            '%environment%/post/',
            'base/post/',
        );

        // Prepare hooks paths
        foreach ($hookPaths as &$hookPath) {
            $hookPath          = $this->getConfig()->parse($hookPath);
            $hookSearchPaths[] = $sourcePath . $hookPath;
        }
        $hookPaths = array_flip($hookPaths);

        // Try to find hooks for given step
        $finder = Finder::create()
            ->files()
            ->name('*.sh')
            ->depth('<= 0')
            ->in($hookSearchPaths);

        // Build base file collection
        foreach ($finder as $partialFile) {

            // Find (un)used hook directories
            array_walk($hookPaths, function(&$item, $key, $haystack){
                if (false !== strpos($haystack, $key)) {
                    $item = true;
                } else {
                    $item = false;
                }
            }, $partialFile);

            // Add to base partial collection
            $basePartialCollection[] = $partialFile;
        }

        // Build installer for all slaves
        foreach ($slaves as $slaveName => $slaveConfig) {

            $slaveHookPath = $this->getConfig()->parse(sprintf(
                '%%environment%%/slaves/%s.sh', $slaveName
            ));

            $installerName = $this->getConfig()->parse(sprintf(
                'installer-%%environment%%-%s.sh', $slaveName
            ));

            // Build paths
            $installerPaths[$slaveName] = $destinationPath . $installerName;
            $partialCollection = $basePartialCollection;

            // On existing slave-hook path just add it
            if (true === file_exists($sourcePath . $slaveHookPath)) {
                $partialCollection[] = $sourcePath . $slaveHookPath;
            } else {
            }

            // Build stub
            $rawStub = array(
                'environment'         => '%environment%',
                'slave_name'          => $slaveName,
                'slave_host'          => $slaveConfig->host,
                'slave_port'          => $slaveConfig->port,
                'package_path'        => $slaveConfig->packageDestination . '%package.filename%',
                'installer_path'      => $slaveConfig->packageDestination . $installerName,
                'project_destination' => $slaveConfig->projectDestination,
                'package_destination' => $slaveConfig->packageDestination
            );
            $stub = '';

            foreach ($rawStub as $varName => &$varValue) {
                $varValue = $this->getConfig()->parse($varValue);
                $stub .= sprintf("%s=\"%s\"\n", $varName, $varValue);
            }

            $stub = sprintf("%s\n", $stub);

            // Setup new bash compiler and add found hooks
            $compiler = new BashCompiler($installerPaths[$slaveName]);
            $compiler->setOutput($this->getOutput())
                     ->setOutputPrefix($this->getOutputPrefix());

            // Add a files to the compilation
            $compiler->addFile($partialCollection);
            $compiler->setStub($stub);
            $compiler->compile();

            $compilerStack[] = $compiler;
        }

        // Write used/unused paths
        $width = max(array_map('strlen', array_keys($hookPaths)));

        if (true === $this->getConfig()->get('packaging.installerCompiler.printUsedPartials')) {
            foreach ($hookPaths as $path => $flag) {
                if (false === $flag) {
                    $this->getOutput()->writeln(
                        sprintf($this->getOutputPrefix()."<br>[</br> <error> Not Used </error> <br>]</br> > <notice>%-${width}s</notice>  ", $path)
                    );
                } else {
                    $this->getOutput()->writeln(
                        sprintf($this->getOutputPrefix()."<br>[</br> <success>   Used   </success> <br>]</br> > <notice>%-${width}s</notice>  ", $path)
                    );
                }
            }
        }

        if (false === array_search(true, $hookPaths)) {
            throw new \RuntimeException(
                'No installer convention path was used. No installer was compiled.'
            );
        }

        // Validate the compiled installers
        if (true === $this->getConfig()->get('packaging.installerCompiler.validateInstaller')) {
            foreach ($compilerStack as $compiler) {
                $compiler->validate();
            }
        }

        // Add compiled installers to the runtime parameters
        $this->getConfig()->addRuntimeParameter('installer', $installerPaths);
    }
}

