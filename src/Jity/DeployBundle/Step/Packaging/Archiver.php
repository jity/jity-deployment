<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Step\Packaging;

use JMS\DiExtraBundle\Annotation as DI;
use Jity\DeployBundle\Step\AbstractStep;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Process\Process;

/**
 * Archiver
 *
 * @DI\Service("jity.deploy.packaging.archiver")
 *
 * @uses   AbstractStep
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class Archiver extends AbstractStep
{
    /**
     * __construct
     *
     * @access public
     * @return void
     */
    public function configure()
    {
        $this
            ->setName('archiver')
            ->setDescription('Compress the source distribution to an archiv')
            ->setCleanup(function() {

                // Remove the fetcher destination
                $this->getFilesystem()->remove(
                    $this->getConfig()->get('packaging.archiver.destination')
                );
            });
    }

    /**
     * getConfiguration
     *
     * Define the configration for the step.
     *
     * @abstract
     * @access public
     * @return Symfony\Component\Config\Definition\Builder\TreeBuilder
     */
    public function getConfiguration()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode    = $treeBuilder->root('archiver');

        $rootNode
            ->addDefaultsIfNotSet()
            ->children()
                ->scalarNode('destination')
                    ->defaultValue('%jity.cache_dir%/packages')
                ->end()
                ->scalarNode('extraCliArgs')
                    ->defaultValue(null)
                ->end()
                ->enumNode('format')
                    ->values(array('gzip', 'bzip2', 'xz'))
                    ->defaultValue('gzip')
                ->end()
                ->scalarNode('verbose')
                    ->defaultFalse()
                ->end()
            ->end();

        return $rootNode;
    }

    /**
     * validate
     *
     * Validate the running conditions of the step.
     * If we dont find problems, we could run and return
     * an empty array, else we will return a array with exceptions.
     *
     * @access public
     * @return array
     */
    public function validate()
    {
        // This step is required but if we find
        // an empty fetcher destination path we cry at runtime
        return array();
    }

    /**
     * getHookPaths
     *
     * Return all locations to look for hooks for the current step.
     *
     * @access public
     * @return array
     */
    public function getHookPaths()
    {
        return array(
            'pre' => array(
                '%jity.config_dir%/hooks/archiver/base',
                '%jity.config_dir%/hooks/archiver/%environment%'
            )
        );
    }

    /**
     * execute
     *
     * Execute statements for the step.
     *
     * @abstract
     * @access public
     * @return void
     */
    public function execute()
    {
        $fetcherDestPath = $this->getConfig()->get('packaging.fetcher.destination');
        $destinationPath = $this->getConfig()->get('packaging.archiver.destination');

        if (true === $this->isEmptyDirectory($fetcherDestPath)) {

            throw new \RuntimeException(sprintf(
                'Fetcher Destination Path "%s" is empty. Nothing to compress.', $fetcherDestPath
            ));
        }

        // Check if we have a valid archiver destination path
        if (false === file_exists($destinationPath)) {

            // Create the destination if needed
            $this->getFilesystem()->mkdir(
                $destinationPath
            );
        }

        // Build local copy process
        $procBuilder = $this->getProcessHelper()->getProcessBuilder(array(
            'tar', 'cfv' . $this->getConfig()->getRuntimeParameter('archiver.tar.switch')
        ));

        $procBuilder
            ->add($this->getConfig()->getRuntimeParameter('package.path'))
            ->add('--directory=' . $fetcherDestPath)
            ->add('.');

        $extraArgs = $this->getConfig()->get(
            'packaging.archiver.extraCliArgs'
        );

        $procBuilder->add('--exclude=.git');

        if (null !== $extraArgs) {

            // Add extra process arguments
            $procBuilder->add($extraArgs);
        }

        // Use abstract process runner helper
        $this->getProcessHelper()->run(
            $procBuilder->getProcess(),
            $this->getConfig()->get('packaging.archiver.verbose')
        );

        // Track the package size
        $this->getConfig()->addRuntimeParameter(
            'package.size',
            filesize($this->getConfig()->getRuntimeParameter('package.path'))
        );
    }
}

