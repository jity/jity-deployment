<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Step\Packaging\Fetcher;

use Jity\DeployBundle\Step\AbstractStep;

/**
 * AbstractFetcher
 *
 * @uses   AbstractStep
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
abstract class AbstractFetcher extends AbstractStep
{
    /**
     * getHookPaths
     *
     * Return all locations to look for hooks for the current step.
     *
     * @access public
     * @return array
     */
    public function getHookPaths()
    {
        return array(
            'post' => array(
                '%jity.config_dir%/hooks/fetcher/base',
                '%jity.config_dir%/hooks/fetcher/base/%fetcher%',
                '%jity.config_dir%/hooks/fetcher/%environment%/base',
                '%jity.config_dir%/hooks/fetcher/%environment%/%fetcher%'
            )
        );
    }

    /**
     * validate
     *
     * Validate the running conditions of the step.
     * If we dont find problems, we could run and return
     * an empty array, else we will return a array with exceptions.
     *
     * @access public
     * @return array
     */
    public function validate()
    {
        $errors          = array();
        $destinationPath = $this->getConfig()->get('packaging.fetcher.destination');

        if (false === $this->isEmptyDirectory($destinationPath)) {

            $errors[] = new \RuntimeException(sprintf(
                'Destination Path "%s" is not empty.', $destinationPath
            ));
        }

        return $errors;
    }
}

