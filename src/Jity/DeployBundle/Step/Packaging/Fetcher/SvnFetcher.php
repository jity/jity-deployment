<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Step\Packaging\Fetcher;

use JMS\DiExtraBundle\Annotation as DI;
use Jity\DeployBundle\Step\AbstractStep;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Process\Process;

/**
 * SvnFetcher
 *
 * @DI\Service
 * @DI\Tag("jity.deploy.packaging.fetcher")
 *
 * @uses   AbstractStep
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class SvnFetcher extends AbstractFetcher
{
    /**
     * __construct
     *
     * @access public
     * @return void
     */
    public function configure()
    {
        $this
            ->setName('fetcher.svn')
            ->setDescription('Loads a copy of the source distribution from a svn repository')
            ->setCleanup(function() {

                // Remove the fetcher destination
                $this->getFilesystem()->remove(
                    $this->getConfig()->get('packaging.fetcher.destination')
                );
            });
    }

    /**
     * getConfiguration
     *
     * Define the configration for the step.
     *
     * @abstract
     * @access public
     * @return Symfony\Component\Config\Definition\Builder\TreeBuilder
     */
    public function getConfiguration()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode    = $treeBuilder->root('svn');

        $rootNode
            ->children()
                ->scalarNode('extraCliArgs')
                    ->defaultValue(null)
                ->end()
                ->arrayNode('source')
                    ->isRequired()
                    ->requiresAtLeastOneElement()
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                    ->children()
                        ->scalarNode('repository')
                            ->isRequired()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ->end();

        return $rootNode;
    }

    /**
     * execute
     *
     * Execute statements for the step.
     *
     * @abstract
     * @access public
     * @return void
     */
    public function execute()
    {
        // Create the fetcher destination
        $this->getFilesystem()->mkdir(
            $this->getConfig()->get('packaging.fetcher.destination')
        );

        // Build local copy process
        $procBuilder = $this->getProcessHelper()->getProcessBuilder(array(
            'svn', 'export'
        ));

        // Add Source
        $procBuilder->add(
            $this->getConfig()->get(
                'packaging.fetcher.sources.svn.source.'
                . $this->getDeployEnvironment() . '.repository'
            )
        );

        // Add Destination
        $procBuilder->add(
            $this->getConfig()->get('packaging.fetcher.destination')
        );

        // Add default options
        $procBuilder->add('--force')->add('--non-interactive');

        $extraArgs = $this->getConfig()->get(
            'packaging.fetcher.sources.svn.extraCliArgs'
        );

        if (null !== $extraArgs) {

            // Add extra process arguments
            $procBuilder->add($extraArgs);
        }

        // Use abstract process runner helper
        $this->getProcessHelper()->run(
            $procBuilder->getProcess(),
            $this->getConfig()->get('packaging.fetcher.verbose')
        );
    }
}

