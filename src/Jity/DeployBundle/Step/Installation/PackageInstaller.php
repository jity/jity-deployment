<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Step\Installation;

use JMS\DiExtraBundle\Annotation as DI;
use Jity\DeployBundle\Step\AbstractStep;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Process\Process;

/**
 * PackageInstaller
 *
 * @DI\Service("jity.deploy.installation.package.installer")
 *
 * @uses   AbstractStep
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class PackageInstaller extends AbstractStep
{
    /**
     * __construct
     *
     * @access public
     * @return void
     */
    public function configure()
    {
        $this
            ->setName('package.installer')
            ->setDescription('Install the transfered packages on slaves');
    }

    /**
     * getConfiguration
     *
     * Define the configration for the step.
     *
     * @abstract
     * @access public
     * @return Symfony\Component\Config\Definition\Builder\TreeBuilder
     */
    public function getConfiguration()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode    = $treeBuilder->root('packageInstaller');

        return $rootNode;
    }

    /**
     * validate
     *
     * Validate the running conditions of the step.
     * If we dont find problems, we could run and return
     * an empty array, else we will return a array with exceptions.
     *
     * @access public
     * @return array
     */
    public function validate()
    {
        // This step is required but if we find
        // an empty fetcher destination path we cry at runtime
        return array();
    }

    /**
     * execute
     *
     * Execute statements for the step.
     *
     * @abstract
     * @access public
     * @return void
     */
    public function execute()
    {
        $processes      = array();
        $environment    = $this->getDeployEnvironment();
        $slaves         = $this->getConfig()->get("slaves.${environment}");
        $installerPaths = $this->getConfig()->getRuntimeParameter('installer');

        // Build copy processes for slaves
        foreach ($slaves as $slaveName => $slaveConfig) {

            $installerPath = sprintf(
                '%s/installer-%s-%s.sh',
                $slaveConfig->packageDestination,
                $environment,
                $slaveName
            );

            // Copy base- to slave-installer script
            $processes[] = $this->getProcessHelper()->buildProcess(array(

                // Connect to slave command
                'ssh', '-p', $slaveConfig->port,
                $slaveConfig->host,

                // Command to install package
                sprintf(
                    '/bin/bash %s',
                    $installerPath,
                    $installerPath
                )
            ));
        }

        // Run the processes parallel, not verbose and dont stop on errors
        $outputs = $this->getProcessHelper()->run($processes, false, true, false);

        // Process throuh all outputs to find errors
        foreach ($outputs as $output) {

            if (false === empty($output['error'])) {
                $this->getOutput()->writeln(sprintf('<error>%s</error>', $output['error']));
            }
        }
    }
}

