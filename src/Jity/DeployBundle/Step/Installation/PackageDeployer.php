<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\Step\Installation;

use JMS\DiExtraBundle\Annotation as DI;
use Jity\DeployBundle\Step\AbstractStep;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Process\Process;

/**
 * PackageDeployer
 *
 * @DI\Service("jity.deploy.installation.package.deployer")
 *
 * @uses   AbstractStep
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class PackageDeployer extends AbstractStep
{
    /**
     * __construct
     *
     * @access public
     * @return void
     */
    public function configure()
    {
        $this
            ->setName('package.deployer')
            ->setDescription('Copy the compressed source distribution to the slaves');
    }

    /**
     * getConfiguration
     *
     * Define the configration for the step.
     *
     * @abstract
     * @access public
     * @return Symfony\Component\Config\Definition\Builder\TreeBuilder
     */
    public function getConfiguration()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode    = $treeBuilder->root('packageDeployer');

        return $rootNode;
    }

    /**
     * validate
     *
     * Validate the running conditions of the step.
     * If we dont find problems, we could run and return
     * an empty array, else we will return a array with exceptions.
     *
     * @access public
     * @return array
     */
    public function validate()
    {
        // This step is required but if we find
        // an empty fetcher destination path we cry at runtime
        return array();
    }

    /**
     * execute
     *
     * Execute statements for the step.
     *
     * @abstract
     * @access public
     * @return void
     */
    public function execute()
    {
        $processes      = array();
        $environment    = $this->getDeployEnvironment();
        $slaves         = $this->getConfig()->get("slaves.${environment}");
        $installerPaths = $this->getConfig()->getRuntimeParameter('installer');
        $packagePath    = $this->getConfig()->getRuntimeParameter('package.path');

        // Build copy processes for slaves
        foreach ($slaves as $slaveName => $slaveConfig) {

            // Copy base- to slave-installer script
            $processes[] = $this->getProcessHelper()->buildProcess(array(

                // Command
                'scp', '-P', $slaveConfig->port,

                // Files to transfer
                $packagePath,
                $installerPaths[$slaveName],

                // Destination
                $slaveConfig->host . ':' . $slaveConfig->packageDestination

            ));
        }

        // Run the processes parallel and not verbose
        $output = $this->getProcessHelper()->run($processes, false, true);
    }
}

