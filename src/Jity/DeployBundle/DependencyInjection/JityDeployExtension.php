<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\DependencyInjection;

use Jity\DeployBundle\DependencyInjection\Configuration\Configuration;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * JityDeployExtension
 *
 * This is the class that loads and manages the bundle configuration.
 *
 * @uses   Extension
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 * @link   http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class JityDeployExtension extends Extension
{
    /**
     * load
     *
     * Load the configuration for the bundle.
     *
     * @param array            $configs   DI Injected Configurations
     * @param ContainerBuilder $container ContainerBuilder Instance
     *
     * @access public
     * @return void
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        // We need to load the services before we can process
        // our configuration, its complicated. Some parts of
        // the configuration are too dynamically, so we need this
        // step first to handle these parts.
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }
}

