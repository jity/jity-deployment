<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\DependencyInjection\Handler\Packaging;

use JMS\DiExtraBundle\Annotation as DI;
use Jity\DeployBundle\DependencyInjection\Handler\AbstractHandler;

/**
 * Handler
 *
 * Task handler.
 *
 * @DI\Service("jity.handler.packaging.fetcher")
 *
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class FetcherHandler extends AbstractHandler
{
}

