<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\DependencyInjection\Handler;

/**
 * AbstractHandler
 *
 * Abstract item handler.
 *
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class AbstractHandler
{
    private $items = array();


    /**
     * add
     *
     * Add a item to the handler.
     *
     * @param mixed $item Item to add
     *
     * @access public
     * @return AbstractHandler
     */
    public function add($item)
    {
        $this->items[$item->getName()] = $item;

        return $this;
    }

    /**
     * has
     *
     * Check if the handler got a given item.
     *
     * @param string $item Name of the item
     *
     * @access public
     * @return boolean
     */
    public function has($item)
    {
        if (array_key_exists((string) $item, $this->items)) {

            return true;
        }

        return false;
    }

    /**
     * get
     *
     * Get a specified task from the handler.
     *
     * @param string $item Name of the item
     *
     * @access public
     * @return mixed
     */
    public function get($item)
    {
        if (array_key_exists((string) $item, $this->items)) {

            return $this->items[(string) $item];
        }
    }

    /**
     * getAllRegistered
     *
     * Get all items which are registered to the handler.
     *
     * @access public
     * @return array
     */
    public function getAllRegistered()
    {
        return $this->items;
    }
}

