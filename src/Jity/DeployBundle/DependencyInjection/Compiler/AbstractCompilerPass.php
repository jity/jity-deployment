<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * AbstractCompilerPass
 *
 * Abstract compiler pass to collect tagged services into common handlers.
 *
 * @uses   CompilerPassInterface
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class AbstractCompilerPass implements CompilerPassInterface
{
    protected $tag;
    protected $handler;

    /**
     * __construct
     *
     * Initalize a new AbstractCompilerPass instance.
     *
     * @param string $handler Canonical Handler Name
     * @param string $tag     Canonical Tag Name
     *
     * @access public
     * @return void
     */
    public function __construct($handler, $tag)
    {
        $this->handler = $handler;
        $this->tag     = $tag;
    }

    /**
     * process
     *
     * Collect all Classes which are tagged as a extension task.
     *
     * @param ContainerBuilder $container Dependency Injection Container
     *
     * @access public
     * @return void
     */
    public function process(ContainerBuilder $container)
    {
        if (null === $this->tag || null === $this->handler) {
            throw new \Exception(
                'You must set a tag and a handler at construct time before '
                . 'the compiler pass can be processed.'
            );

            return false;
        }

        if (!$container->hasDefinition($this->handler)) {
            return;
        }

        $definition = $container->getDefinition(
            $this->handler
        );

        $taggedServices = $container->findTaggedServiceIds(
            $this->tag
        );

        // Avoiding unused variables, so if we need attributes:
        // foreach ($taggedServices as $id => $attributes) {
        foreach (array_keys($taggedServices) as $id) {

            $definition->addMethodCall(
                'add',
                array(new Reference($id))
            );
        }
    }
}

