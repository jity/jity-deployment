<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\DependencyInjection\Configuration\Deploy;

use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * ConfigStatic
 *
 * Static deployment configuration tree.
 *
 * @uses   ConfigurationInterface
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class ConfigStatic implements ConfigurationInterface
{
    /**
     * @var container
     * @access protected
     */
    protected $container;


    /**
     * __construct
     *
     * Initalize a new AbstractService instance.
     *
     * @param ContainerInterface $container Dependency Injected DI Container
     *
     * @access public
     * @return void
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * getConfigTreeBuilder
     *
     * Build a validation tree for a given configuration processor.
     *
     * @access public
     * @return Symfony\Component\Config\Definition\Builder\TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('deployment');

        $rootNode
            ->children()
                // We validate this sub-tree later
                ->variableNode('slaves')->end()
                ->append($this->addDefaultsNode())
                ->append($this->addPackagingNode())
            ->end();

        // Sadly the node tree builder has no options
        // to append an array of nodes under a node.
        // With this in mind we have to build the tree,
        // extract the 'fetcher' root node and add children
        // in a loop to it.
        $buildedNodes  = $treeBuilder->buildTree()->getChildren();
        $packagingNode = $buildedNodes['packaging']->getChildren();
        $fetcherNode   = $packagingNode['fetcher']->getChildren();
        $sourcesNode   = $fetcherNode['sources'];

        // Get all services with the fetcher tag
        $fetcher = $this->container->get('jity.handler.packaging.fetcher');

        foreach ($fetcher->getAllRegistered() as $fetcher) {

            // Extract the service configuration and prepare it for appending
            $child = $fetcher->getConfiguration()->getNode(true);

            // Append the service configuration node to the fetcher root node
            $sourcesNode->addChild($child);
        }

        return $treeBuilder;
    }

    /**
     * addDefaultsNode
     *
     * Add menu configuration validation tree.
     *
     * @access private
     * @return Symfony\Component\Config\Definition\Builder\TreeBuilder
     */
    private function addDefaultsNode()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('defaults');

        $rootNode
            ->children()
                ->scalarNode('environment')
                    ->defaultValue(null)
                ->end()
                ->scalarNode('source')
                    ->defaultValue(null)
                ->end()
                ->booleanNode('printUsedCommands')
                    ->defaultValue(false)
                ->end()
                ->booleanNode('verboseHooks')
                    ->defaultValue(false)
                ->end()
            ->end();

        return $rootNode;
    }

    /**
     * addPackagingNode
     *
     * Add packaging configuration validation tree.
     *
     * @access private
     * @return Symfony\Component\Config\Definition\Builder\TreeBuilder
     */
    private function addPackagingNode()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('packaging');

        $rootNode
            ->children()
                ->arrayNode('fetcher')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('destination')
                            ->defaultValue('%jity.cache_dir%/distribution')
                        ->end()
                        ->scalarNode('verbose')
                            ->defaultFalse()
                        ->end()
                        ->arrayNode('sources')
                            ->children()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->append(
                    $this->container->get('jity.deploy.packaging.fixtures.loader')
                    ->getConfiguration()
                )
                ->append(
                    $this->container->get('jity.deploy.packaging.installer.compiler')
                    ->getConfiguration()
                )
                ->append(
                    $this->container->get('jity.deploy.packaging.archiver')
                    ->getConfiguration()
                )
            ->end();

        return $rootNode;
    }
}

