<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\DependencyInjection\Configuration\Deploy;

use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

/**
 * ConfigDynamic
 *
 * Dynamic deployment configuration tree.
 *
 * @uses   ConfigurationInterface
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class ConfigDynamic implements ConfigurationInterface
{
    /**
     * getConfigTreeBuilder
     *
     * Build a validation tree for a given configuration processor.
     *
     * @access public
     * @return Symfony\Component\Config\Definition\Builder\TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('slaves');

        $rootNode
            ->isRequired()
            ->useAttributeAsKey('name')
            ->requiresAtLeastOneElement()
            ->prototype('array')
                ->children()

                    ->scalarNode('host')
                        ->isRequired()
                    ->end()

                    ->integerNode('port')
                        ->defaultValue(22)
                    ->end()

                    ->scalarNode('projectDestination')
                        ->isRequired()
                    ->end()

                    ->scalarNode('packageDestination')
                        ->isRequired()
                    ->end()

                ->end()
            ->end()

        ->end();

        return $treeBuilder;
    }
}

