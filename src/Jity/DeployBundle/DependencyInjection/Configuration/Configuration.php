<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\DependencyInjection\Configuration;

use Symfony\Component\Config\Definition\Processor;
use Jity\DeployBundle\DependencyInjection\Configuration\Deploy\ConfigStatic;
use Jity\DeployBundle\DependencyInjection\Configuration\Deploy\ConfigDynamic;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\Yaml\Yaml;

/**
 * Configuration
 *
 * Configuration definition for deployment.
 *
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class Configuration
{
    protected $container;
    protected $config;
    protected $propertyAccessor;
    protected $runtimeParameters;


    /**
     * __construct
     *
     * Initalize a new DeployConfig instance.
     *
     * @param ContainerInterface $container Dependency Injected DI Container
     *
     * @access public
     * @return void
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $paths           = $container->getParameter('jity.config.paths');
        $foundPath       = false;

        // Search for a valid configration file
        foreach ($paths as $path) {
            if (file_exists($path)) {
                $foundPath = $path;
                break;
            }
        }

        if (false === $foundPath) {

            throw new \RuntimeException(
                sprintf(
                    "None of these configration files was found:\n%s",
                    implode("\n", $paths)
                )
            );
        }

        $this->config = $this->process(Yaml::parse($foundPath));

        $convention = new Convention($this);
        $this->runtimeParameters = $convention->build();
    }

    /**
     * get
     *
     * Get a property from the configration.
     *
     * @param string $path Property path to retrieve
     *
     * @access public
     * @return mixed
     */
    public function get($path = null)
    {
        // In case we have no path, return all
        if (null === $path) {
            return $this->config;
        }

        // If the path contains % chars, try to parse it
        if (is_string($path) && false !== strpos($path, '%')) {
            $path = $this->parseValue($path);
        }

        // Lazy load property accessor
        if (null === $this->propertyAccessor) {
            $this->propertyAccessor = new PropertyAccessor();
        }

        $value = $this->propertyAccessor->getValue($this->config, $path);

        // If the value contains % chars, try to parse it
        if (is_string($value) && false !== strpos($value, '%')) {
            return $this->parseValue($value);
        }

        return $value;
    }

    /**
     * parse
     *
     * Parse a configration property for additional %parameters%.
     *
     * @param string $path       Property path to parse
     * @param array  $parameters Optional Parameters
     *
     * @access public
     * @return mixed
     */
    public function parse($path, array $parameters = array())
    {
        // We dont want integers to be parsed
        if (is_numeric($path)) {
            return $path;
        }

        try {

            // Try to get value for the path
            $value = $this->get($path);

        } catch (\Exception $e) {

            // On path errors, check if the path is a string
            // otherwise just rethrow the exception
            if (!is_string($path)) {
                throw $e;
            }
        }

        return $this->parseValue($path, $parameters);
    }

    /**
     * getRuntimeParameter
     *
     * Get registered runtime parameter by name.
     *
     * @param string $name Name of the runtime parameter
     *
     * @access public
     * @return mixed
     */
    public function getRuntimeParameter($name)
    {
        return $this->runtimeParameters[$name];
    }

    /**
     * getRuntimeParameters
     *
     * Get all registered runtime parameters.
     *
     * @access public
     * @return array
     */
    public function getRuntimeParameters()
    {
        return $this->runtimeParameters;
    }

    /**
     * addRuntimeParameter
     *
     * Add one parameter to the runtime parameters.
     *
     * @param string $name  Name of the parameter
     * @param mixed  $value Value of the parameter
     *
     * @access public
     * @return Configuration
     */
    public function addRuntimeParameter($name, $value)
    {
        $this->runtimeParameters[$name] = $value;

        return $this;
    }

    /**
     * process
     *
     * Get a processed/validated configuration array from a configuration file
     *
     * @param string $config Configuration file content to process
     *
     * @access public
     * @return array
     */
    private function process($config)
    {
        $processor    = new Processor();
        $staticConfig = new ConfigStatic($this->container);

        // Just validate the static tree
        $processedConfig = $processor->processConfiguration(
            $staticConfig,
            $config
        );

        // Configure dynamic configuration tree
        $dynamicConfig = new ConfigDynamic();
        $environments  = array();

        // Validate all slaves
        foreach ($processedConfig['slaves'] as $environment => $slaves) {

            $environments[] = $environment;

            $processedSlaves = $processor->processConfiguration(
                $dynamicConfig,
                array('slaves' => $slaves)
            );

            // Remove unvalidated environment configuration
            unset($processedConfig['slaves'][$environment]);

            // Rewrite validated slaves for environment
            $processedConfig['slaves'][$environment] = $processedSlaves;
        }

        // Add environments to the processed configuration
        $processedConfig['environments'] = $environments;

        // Search for paths and normalize them
        array_walk_recursive($processedConfig, function (&$item, $key){

            // Dont normalize urls
            if (false !== strpos($item, '://')) {

                return;
            }

            // Search for a slash in item, if we found one it
            // seems to be an path so normalize it
            if (false !== strpos($item, '/')) {

                $item = preg_replace('/[\/]{2,}/', '/', $item . '/');
            }
        });

        // Convert multidimensional array to StdClass structure
        return json_decode(json_encode(
            $processedConfig
        ));
    }

    /**
     * parseValue
     *
     * Parse value for parameters from dependency injection
     * container or from a specified parameters array.
     *
     * @param string $value      Value to parse
     * @param array  $parameters Optional Parameters
     *
     * @access private
     * @return mixed
     */
    private function parseValue($value, array $parameters = array())
    {
        // This is that for closure context
        $that = $this;

        // Search and Replace %parameters%
        return preg_replace_callback(
            '/%([^%]*)%/',
            function($match) use ($that, $parameters) {

                $param = $match[1];

                // Check the specified parameters list, if specified,
                // for the parameter
                if (!empty($parameters) && array_key_exists($param, $parameters)) {
                    return $parameters[$param];
                }

                // Check the registered runtime parameters list
                // for the parameter
                if (!empty($this->runtimeParameters)
                    && array_key_exists($param, $this->runtimeParameters)) {

                    return $this->runtimeParameters[$param];
                }

                // Check the dependency injection container
                // for the parameter
                if ($that->container->hasParameter($param)) {
                    return $that->container->getParameter($param);
                }

                // Nothing found, just return the unreplaced parameter
                return $match[0];
            },
            $value
        );
    }
}

