<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\DeployBundle\DependencyInjection\Configuration;

use Symfony\Component\Config\Definition\Processor;
use Jity\DeployBundle\DependencyInjection\Configuration\Deploy\ConfigStatic;
use Jity\DeployBundle\DependencyInjection\Configuration\Deploy\ConfigDynamic;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\Yaml\Yaml;

/**
 * Convention
 *
 * Build default variable by convention based of configration.
 *
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class Convention
{
    private $config;


    /**
     * __construct
     *
     * Initalize a new instance of Convention.
     *
     * @param Configuration $configuration Configuration to use
     *
     * @access public
     * @return void
     */
    public function __construct(Configuration $configuration)
    {
        $this->config = $configuration;
    }

    /**
     * build
     *
     * Build defaults by convention and configration.
     *
     * @access public
     * @return array
     */
    public function build()
    {
        $defaults = array();

        // Build package format mapping
        switch ($format = $this->config->get('packaging.archiver.format')) {
            case 'gzip':
                $packageFilename = 'package.tar.gz';
                $tarSwitch = 'z';
                break;
            case 'bzip2':
                $packageFilename = 'package.tar.bz2';
                $tarSwitch = 'j';
                break;
            case 'xz':
                $packageFilename = 'package.tar.xz';
                $tarSwitch = 'J';
                break;
            default:
                throw new \RuntimeException(
                    sprintf('Format "%s" not handled by Archiver.', $format)
                );
                break;
        }

        $defaults['package.filename']    = $packageFilename;
        $defaults['archiver.tar.switch'] = $tarSwitch;

        $defaults['package.path'] = $this->config
            ->get('packaging.archiver.destination') . '/' . $packageFilename;

        return $defaults;
    }
}

