<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;
use JMS\DiExtraBundle\Exception\RuntimeException;

/**
 * AppKernel
 *
 * @uses   Kernel
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class AppKernel extends Kernel
{
    /**
     * registerBundles
     *
     * @access public
     * @return void
     */
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new JMS\AopBundle\JMSAopBundle(),
            new JMS\DiExtraBundle\JMSDiExtraBundle($this),
            new Jity\DeployBundle\JityDeployBundle(),
        );

        return $bundles;
    }

    /**
     * registerContainerConfiguration
     *
     * @param LoaderInterface $loader
     *
     * @access public
     * @return void
     */
    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config.yml');
    }

    /**
     * getCacheDir
     *
     * Return the default cache directory.
     *
     * @access public
     * @return string
     */
    public function getCacheDir()
    {
        return '/tmp/jity/cache-' . crc32(getcwd());
    }

    /**
     * getLogDir
     *
     * Return the default logs directory.
     *
     * @access public
     * @return string
     */
    public function getLogDir()
    {
        return getcwd() . '/deployment/logs';
    }
}

