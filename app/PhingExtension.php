<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

if (file_exists(__DIR__.'/bootstrap.php.cache')) {
    require_once __DIR__.'/bootstrap.php.cache';
} else {
    require_once __DIR__.'/autoload.php';
}

require_once __DIR__.'/AppKernel.php';
require_once 'phing/Task.php';

/**
 * PhingExtension
 *
 * Description
 *
 * @uses   Task
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class PhingExtension extends Task
{
    private static $kernel;
    private static $cleanupQueue  = array();

    /**
     * init
     *
     * Boot the kernel.
     *
     * @access public
     * @return void
     */
    public function init()
    {
        // Bootstrap the application kernel
        self::$kernel = new AppKernel('dev', true);
        self::$kernel->boot();
    }

    /**
     * main
     *
     * Register all task/step wrappers.
     *
     * @access public
     * @return void
     */
    public function main()
    {
        $taskHandler = self::$kernel->getContainer()->get('jity.handler.phing.task');

        // Loop through registered phing tasks and register them
        foreach ($taskHandler->getAllRegistered() as $name => $task) {

            // print('     Registered Task: ' . $name . PHP_EOL);

            $this->project->addTaskDefinition($name, get_class($task));
        }
    }

    /**
     * getKernel
     *
     * Get the booted kernel from everywhere.
     * This method is used to prepare the steps.
     *
     * @static
     * @access public
     * @return Kernel
     */
    public static function getKernel()
    {
        return self::$kernel;
    }

    /**
     * getCleanupQueue
     *
     * Return the static saved cleanup queue.
     *
     * @static
     * @access public
     * @return array
     */
    public static function getCleanupQueue()
    {
        return self::$cleanupQueue;
    }

    /**
     * addTaskToCleanupQueue
     *
     * Add a task to the cleanup queue.
     *
     * @param mixed $task Task to add
     *
     * @static
     * @access public
     * @return void
     */
    public static function addTaskToCleanupQueue($name, $task)
    {
        self::$cleanupQueue[$name] = $task;
    }
}

